﻿using CarServiceWCFService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFHost
{
    class WCFHost
    {
        static ServiceHost studentServiceHost = null;

        public static void Main(string[] args)
        {
            try
            {
                Uri httpBaseAddress = new Uri("http://localhost:4321/StudentService");
                studentServiceHost = new ServiceHost(typeof(CarServiceWCFService.CarServiceAPIWCF), httpBaseAddress);
                studentServiceHost.AddServiceEndpoint(typeof(CarServiceWCFService.ICarServiceAPIWCF), new WSHttpBinding(), "");
                ServiceMetadataBehavior serviceBehavior = new ServiceMetadataBehavior();
                serviceBehavior.HttpGetEnabled = true;
                studentServiceHost.Description.Behaviors.Add(serviceBehavior);
                studentServiceHost.Open();
                Console.Out.WriteLine(studentServiceHost.Description.Name);
                Console.ReadKey();
                studentServiceHost.Close();
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
