
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/14/2019 18:57:45
-- Generated from EDMX file: G:\Cosmin\VisualStudio\ModelDesignFirst_Laborator1\ModelDesignFirst_Laborator1\CarServiceDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [tspdotnet-laborator1];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AutoSasiu]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Autos] DROP CONSTRAINT [FK_AutoSasiu];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientAuto]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Autos] DROP CONSTRAINT [FK_ClientAuto];
GO
IF OBJECT_ID(N'[dbo].[FK_ComandaAuto]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comands] DROP CONSTRAINT [FK_ComandaAuto];
GO
IF OBJECT_ID(N'[dbo].[FK_ComandaClient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comands] DROP CONSTRAINT [FK_ComandaClient];
GO
IF OBJECT_ID(N'[dbo].[FK_DetaliuComandaComanda]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DetaliiComenzi] DROP CONSTRAINT [FK_DetaliuComandaComanda];
GO
IF OBJECT_ID(N'[dbo].[FK_DetaliuComandaImagine]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Images] DROP CONSTRAINT [FK_DetaliuComandaImagine];
GO
IF OBJECT_ID(N'[dbo].[FK_DetaliuComandaMaterial]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Materials] DROP CONSTRAINT [FK_DetaliuComandaMaterial];
GO
IF OBJECT_ID(N'[dbo].[FK_DetaliuComandaMecanic]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Mecanics] DROP CONSTRAINT [FK_DetaliuComandaMecanic];
GO
IF OBJECT_ID(N'[dbo].[FK_DetaliuComandaOperatie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_DetaliuComandaOperatie];
GO
IF OBJECT_ID(N'[dbo].[FK_MecanicOperatie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Mecanics] DROP CONSTRAINT [FK_MecanicOperatie];
GO
IF OBJECT_ID(N'[dbo].[FK_StareComandaComanda]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comands] DROP CONSTRAINT [FK_StareComandaComanda];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Autos]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Autos];
GO
IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[Comands]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comands];
GO
IF OBJECT_ID(N'[dbo].[DetaliiComenzi]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DetaliiComenzi];
GO
IF OBJECT_ID(N'[dbo].[Images]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Images];
GO
IF OBJECT_ID(N'[dbo].[Materials]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Materials];
GO
IF OBJECT_ID(N'[dbo].[Mecanics]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mecanics];
GO
IF OBJECT_ID(N'[dbo].[Operations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Operations];
GO
IF OBJECT_ID(N'[dbo].[Sasiuri]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sasiuri];
GO
IF OBJECT_ID(N'[dbo].[StariComanda]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StariComanda];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [ClientId] int IDENTITY(1,1) NOT NULL,
    [Nume] nvarchar(15)  NOT NULL,
    [Prenume] nvarchar(15)  NOT NULL,
    [Adresa] nvarchar(50)  NULL,
    [Localitate] nvarchar(20)  NULL,
    [Judet] nvarchar(20)  NULL,
    [Telefon] nvarchar(13)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- Creating table 'Autos'
CREATE TABLE [dbo].[Autos] (
    [AutoId] int IDENTITY(1,1) NOT NULL,
    [NumarAuto] nvarchar(10)  NOT NULL,
    [SerieSasiu] nvarchar(25)  NOT NULL,
    [ClientId] int  NOT NULL,
    [isDeleted] bit  NOT NULL,
    [Sasiu_SasiuId] int  NOT NULL
);
GO

-- Creating table 'Sasiuri'
CREATE TABLE [dbo].[Sasiuri] (
    [SasiuId] int IDENTITY(1,1) NOT NULL,
    [CodSasiu] nvarchar(2)  NOT NULL,
    [Denumire] nvarchar(25)  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- Creating table 'Mecanics'
CREATE TABLE [dbo].[Mecanics] (
    [MecanicId] int IDENTITY(1,1) NOT NULL,
    [Nume] nvarchar(15)  NOT NULL,
    [Prenume] nvarchar(15)  NOT NULL,
    [DetaliuComandaId] int  NOT NULL,
    [isDeleted] bit  NOT NULL,
    [Operatie_OperatieId] int  NOT NULL
);
GO

-- Creating table 'Materials'
CREATE TABLE [dbo].[Materials] (
    [MaterialId] int IDENTITY(1,1) NOT NULL,
    [Denumire] nvarchar(50)  NOT NULL,
    [Cantitate] decimal(10,2)  NOT NULL,
    [Pret] decimal(10,2)  NOT NULL,
    [DataAprovizionare] datetime  NOT NULL,
    [DetaliuComandaId] int  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- Creating table 'Images'
CREATE TABLE [dbo].[Images] (
    [ImagineId] int IDENTITY(1,1) NOT NULL,
    [Titlu] nvarchar(15)  NOT NULL,
    [Descriere] nvarchar(256)  NOT NULL,
    [Data] datetime  NOT NULL,
    [Foto] varbinary(max)  NOT NULL,
    [DetaliuComandaId] int  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- Creating table 'Operations'
CREATE TABLE [dbo].[Operations] (
    [OperatieId] int IDENTITY(1,1) NOT NULL,
    [Denumire] nvarchar(256)  NOT NULL,
    [TimpExecutie] decimal(6,2)  NOT NULL,
    [DetaliuComandaDetaliuComandaId] int  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- Creating table 'Comands'
CREATE TABLE [dbo].[Comands] (
    [ComandaId] int IDENTITY(1,1) NOT NULL,
    [DataSystem] datetime  NOT NULL,
    [DataProgramare] datetime  NOT NULL,
    [DataFinalizare] datetime  NOT NULL,
    [KmBord] int  NOT NULL,
    [Descriere] nvarchar(1024)  NOT NULL,
    [ValoarePiese] decimal(10,2)  NOT NULL,
    [isDeleted] bit  NOT NULL,
    [Auto_AutoId] int  NOT NULL,
    [Client_ClientId] int  NOT NULL,
    [StareComanda_StareComandaId] int  NOT NULL
);
GO

-- Creating table 'DetaliiComenzi'
CREATE TABLE [dbo].[DetaliiComenzi] (
    [DetaliuComandaId] int IDENTITY(1,1) NOT NULL,
    [isDeleted] bit  NOT NULL,
    [Comanda_ComandaId] int  NOT NULL
);
GO

-- Creating table 'StariComanda'
CREATE TABLE [dbo].[StariComanda] (
    [StareComandaId] int IDENTITY(1,1) NOT NULL,
    [Denumire] nvarchar(max)  NOT NULL,
    [isDeleted] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ClientId] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([ClientId] ASC);
GO

-- Creating primary key on [AutoId] in table 'Autos'
ALTER TABLE [dbo].[Autos]
ADD CONSTRAINT [PK_Autos]
    PRIMARY KEY CLUSTERED ([AutoId] ASC);
GO

-- Creating primary key on [SasiuId] in table 'Sasiuri'
ALTER TABLE [dbo].[Sasiuri]
ADD CONSTRAINT [PK_Sasiuri]
    PRIMARY KEY CLUSTERED ([SasiuId] ASC);
GO

-- Creating primary key on [MecanicId] in table 'Mecanics'
ALTER TABLE [dbo].[Mecanics]
ADD CONSTRAINT [PK_Mecanics]
    PRIMARY KEY CLUSTERED ([MecanicId] ASC);
GO

-- Creating primary key on [MaterialId] in table 'Materials'
ALTER TABLE [dbo].[Materials]
ADD CONSTRAINT [PK_Materials]
    PRIMARY KEY CLUSTERED ([MaterialId] ASC);
GO

-- Creating primary key on [ImagineId] in table 'Images'
ALTER TABLE [dbo].[Images]
ADD CONSTRAINT [PK_Images]
    PRIMARY KEY CLUSTERED ([ImagineId] ASC);
GO

-- Creating primary key on [OperatieId] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [PK_Operations]
    PRIMARY KEY CLUSTERED ([OperatieId] ASC);
GO

-- Creating primary key on [ComandaId] in table 'Comands'
ALTER TABLE [dbo].[Comands]
ADD CONSTRAINT [PK_Comands]
    PRIMARY KEY CLUSTERED ([ComandaId] ASC);
GO

-- Creating primary key on [DetaliuComandaId] in table 'DetaliiComenzi'
ALTER TABLE [dbo].[DetaliiComenzi]
ADD CONSTRAINT [PK_DetaliiComenzi]
    PRIMARY KEY CLUSTERED ([DetaliuComandaId] ASC);
GO

-- Creating primary key on [StareComandaId] in table 'StariComanda'
ALTER TABLE [dbo].[StariComanda]
ADD CONSTRAINT [PK_StariComanda]
    PRIMARY KEY CLUSTERED ([StareComandaId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ClientId] in table 'Autos'
ALTER TABLE [dbo].[Autos]
ADD CONSTRAINT [FK_ClientAuto]
    FOREIGN KEY ([ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientAuto'
CREATE INDEX [IX_FK_ClientAuto]
ON [dbo].[Autos]
    ([ClientId]);
GO

-- Creating foreign key on [Sasiu_SasiuId] in table 'Autos'
ALTER TABLE [dbo].[Autos]
ADD CONSTRAINT [FK_AutoSasiu]
    FOREIGN KEY ([Sasiu_SasiuId])
    REFERENCES [dbo].[Sasiuri]
        ([SasiuId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AutoSasiu'
CREATE INDEX [IX_FK_AutoSasiu]
ON [dbo].[Autos]
    ([Sasiu_SasiuId]);
GO

-- Creating foreign key on [Auto_AutoId] in table 'Comands'
ALTER TABLE [dbo].[Comands]
ADD CONSTRAINT [FK_ComandaAuto]
    FOREIGN KEY ([Auto_AutoId])
    REFERENCES [dbo].[Autos]
        ([AutoId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComandaAuto'
CREATE INDEX [IX_FK_ComandaAuto]
ON [dbo].[Comands]
    ([Auto_AutoId]);
GO

-- Creating foreign key on [Client_ClientId] in table 'Comands'
ALTER TABLE [dbo].[Comands]
ADD CONSTRAINT [FK_ComandaClient]
    FOREIGN KEY ([Client_ClientId])
    REFERENCES [dbo].[Clients]
        ([ClientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComandaClient'
CREATE INDEX [IX_FK_ComandaClient]
ON [dbo].[Comands]
    ([Client_ClientId]);
GO

-- Creating foreign key on [Comanda_ComandaId] in table 'DetaliiComenzi'
ALTER TABLE [dbo].[DetaliiComenzi]
ADD CONSTRAINT [FK_DetaliuComandaComanda]
    FOREIGN KEY ([Comanda_ComandaId])
    REFERENCES [dbo].[Comands]
        ([ComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetaliuComandaComanda'
CREATE INDEX [IX_FK_DetaliuComandaComanda]
ON [dbo].[DetaliiComenzi]
    ([Comanda_ComandaId]);
GO

-- Creating foreign key on [DetaliuComandaId] in table 'Mecanics'
ALTER TABLE [dbo].[Mecanics]
ADD CONSTRAINT [FK_DetaliuComandaMecanic]
    FOREIGN KEY ([DetaliuComandaId])
    REFERENCES [dbo].[DetaliiComenzi]
        ([DetaliuComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetaliuComandaMecanic'
CREATE INDEX [IX_FK_DetaliuComandaMecanic]
ON [dbo].[Mecanics]
    ([DetaliuComandaId]);
GO

-- Creating foreign key on [DetaliuComandaId] in table 'Materials'
ALTER TABLE [dbo].[Materials]
ADD CONSTRAINT [FK_DetaliuComandaMaterial]
    FOREIGN KEY ([DetaliuComandaId])
    REFERENCES [dbo].[DetaliiComenzi]
        ([DetaliuComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetaliuComandaMaterial'
CREATE INDEX [IX_FK_DetaliuComandaMaterial]
ON [dbo].[Materials]
    ([DetaliuComandaId]);
GO

-- Creating foreign key on [DetaliuComandaId] in table 'Images'
ALTER TABLE [dbo].[Images]
ADD CONSTRAINT [FK_DetaliuComandaImagine]
    FOREIGN KEY ([DetaliuComandaId])
    REFERENCES [dbo].[DetaliiComenzi]
        ([DetaliuComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetaliuComandaImagine'
CREATE INDEX [IX_FK_DetaliuComandaImagine]
ON [dbo].[Images]
    ([DetaliuComandaId]);
GO

-- Creating foreign key on [Operatie_OperatieId] in table 'Mecanics'
ALTER TABLE [dbo].[Mecanics]
ADD CONSTRAINT [FK_MecanicOperatie]
    FOREIGN KEY ([Operatie_OperatieId])
    REFERENCES [dbo].[Operations]
        ([OperatieId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MecanicOperatie'
CREATE INDEX [IX_FK_MecanicOperatie]
ON [dbo].[Mecanics]
    ([Operatie_OperatieId]);
GO

-- Creating foreign key on [StareComanda_StareComandaId] in table 'Comands'
ALTER TABLE [dbo].[Comands]
ADD CONSTRAINT [FK_StareComandaComanda]
    FOREIGN KEY ([StareComanda_StareComandaId])
    REFERENCES [dbo].[StariComanda]
        ([StareComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StareComandaComanda'
CREATE INDEX [IX_FK_StareComandaComanda]
ON [dbo].[Comands]
    ([StareComanda_StareComandaId]);
GO

-- Creating foreign key on [DetaliuComandaDetaliuComandaId] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_DetaliuComandaOperatie]
    FOREIGN KEY ([DetaliuComandaDetaliuComandaId])
    REFERENCES [dbo].[DetaliiComenzi]
        ([DetaliuComandaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DetaliuComandaOperatie'
CREATE INDEX [IX_FK_DetaliuComandaOperatie]
ON [dbo].[Operations]
    ([DetaliuComandaDetaliuComandaId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------