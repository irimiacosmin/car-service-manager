﻿using System;
using System.Linq;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class ComandaTests
    {
        private readonly ComandaService _comandaService = new ComandaService();

        [TestMethod]
        public void Comanda_GET_GetById_GoodExample()
        {
            var entity = _comandaService.GetById(1);
            Assert.AreEqual(1, entity.ComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Comanda_GET_GetById_BadExample()
        {
            _comandaService.GetById(0);
        }

        [TestMethod]
        public void Comanda_GET_GetByClientId_GoodExample()
        {
            var entity = _comandaService.GetByClientId(1);
            Assert.AreEqual(37,entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Comanda_GET_GetByClientId_BadExample()
        {
            _comandaService.GetByClientId(0);
        }

        [TestMethod]
        public void Comanda_GET_GetByValoarePiese_GoodExample()
        {
            var entity = _comandaService.GetByValoarePiese(new decimal(1000), new decimal(2000));
            Assert.AreEqual(3460, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Comanda_GET_GetByValoarePiese_BadExample()
        {
            _comandaService.GetByValoarePiese(new decimal(0), new decimal(1));
        }

        [TestMethod]
        public void Comanda_GET_GetByAutoId_GoodExample()
        {
            var entity = _comandaService.GetByAutoId(1);
            Assert.AreEqual(17, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Comanda_GET_GetByAutoId_BadExample()
        {
            _comandaService.GetByAutoId(0);
        }

        [TestMethod]
        public void Comanda_GET_GetByStareComanda_GoodExample()
        {
            var entity = _comandaService.GetByStareComanda(1);
            Assert.AreEqual(3349, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Comanda_GET_GetByStareComanda_BadExample()
        {
            _comandaService.GetByStareComanda(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Comanda_ADD_BadExample()
        {
            _comandaService.Add(_comandaService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Comanda_DELETE_BadExample()
        {
            _comandaService.Delete(0);
        }
    }
}
