﻿using System;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class DetaliiComandaTests
    {
        private readonly DetaliiComandaService _detaliiComandaService = new DetaliiComandaService();

        [TestMethod]
        public void DetaliiComanda_GET_GetById_GoodExample()
        {
            var entity = _detaliiComandaService.GetById(1);
            Assert.AreEqual(1, entity.DetaliuComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void DetaliiComanda_GET_GetById_BadExample()
        {
            _detaliiComandaService.GetById(0);
           
        }

        [TestMethod]
        public void DetaliiComanda_GET_GetByComandaId_GoodExample()
        {
            var entity = _detaliiComandaService.GetByComandaId(1);
            Assert.AreEqual(9505, entity.DetaliuComandaId);

        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void DetaliiComanda_GET_GetByComandaId_BadExample()
        {
            _detaliiComandaService.GetByComandaId(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void DetaliiComanda_ADD_BadExample()
        {
            _detaliiComandaService.Add(_detaliiComandaService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void DetaliiComanda_DELETE_BadExample()
        {
            _detaliiComandaService.Delete(0);
        }
    }
}
