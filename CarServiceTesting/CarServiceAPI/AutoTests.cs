﻿using System;
using System.Linq;
using CarServiceAPI.Services;
using CarServiceAPI.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelDesignFirst_Laborator1;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class AutoTests
    {
        private readonly AutoService _autoService = new AutoService();
        private readonly SasiuService _sasiuService = new SasiuService();

        [TestMethod]
        public void Auto_GET_GetById_GoodExample()
        {
            var auto = _autoService.GetById(1);
            Assert.AreEqual("BH-75-TPA", auto.NumarAuto);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Auto_GET_GetById_BadExample()
        {
            _autoService.GetById(0);
        }

        [TestMethod]
        public void Auto_GET_GetByNumarDeInmatriculare_GoodExample()
        {
            var auto = _autoService.GetByNumarDeInmatriculare("BH-75-TPA");
            Assert.AreEqual(1, auto.AutoId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Auto_GET_GetByNumarDeInmatriculare_BadExample()
        {
            _autoService.GetByNumarDeInmatriculare("ANYTHING");
        }

        [TestMethod]
        public void Auto_GET_GetBySerieSasiu_GoodExample()
        {
            var auto = _autoService.GetBySerieSasiu("TNO0OYXV8M6K373S8");
            Assert.AreEqual(1, auto.AutoId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Auto_GET_GetBySerieSasiu_BadExample()
        {
            _autoService.GetBySerieSasiu("ANYTHING");
        }

        [TestMethod]
        public void Auto_GET_GetByClientId_GoodExample()
        {
            var auto = _autoService.GetByClientId(218);
            Assert.AreEqual(3, auto.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Auto_GET_GetByClientId_BadExample()
        {
            _autoService.GetByClientId(0);
        }

        [TestMethod]
        public void Auto_ADD_DELETE_GoodExample()
        {
            var sasiu = new Sasiu();
            sasiu.CodSasiu = "AA";
            sasiu.Denumire = "TEST";
            sasiu.isDeleted = false;

            var auto = new Auto();
            auto.AutoId = 0;
            auto.NumarAuto = "BT-00-AAA";
            auto.Sasiu = sasiu;
            auto.SerieSasiu = "TEST";
            auto.ClientId = 218;
            auto.isDeleted = false;

            var newAuto = _autoService.Add(auto);
            Assert.AreEqual(auto, newAuto);
            _autoService.Delete(newAuto.AutoId);
            _sasiuService.Delete(newAuto.Sasiu.SasiuId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Auto_DELETE_BadExample()
        {
            _autoService.Delete(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Auto_ADD_BadExample()
        {
            var auto = _autoService.GetById(1);
            _autoService.Add(auto);
        }

        [TestMethod]
        public void Auto_UPDATE_GoodExample()
        {
            var sasiu = new Sasiu();
            sasiu.CodSasiu = "AA";
            sasiu.Denumire = "TEST";
            sasiu.isDeleted = false;

            var auto = new Auto();
            auto.AutoId = 0;
            auto.NumarAuto = "BT-00-AAA";
            auto.Sasiu = sasiu;
            auto.SerieSasiu = "TEST";
            auto.ClientId = 218;
            auto.isDeleted = false;

            var newAuto = _autoService.Add(auto);
            auto.NumarAuto = "BT-00-BBB";
            _autoService.Update(auto);

            var updatedAuto = _autoService.GetById(newAuto.AutoId);
            Assert.AreEqual(updatedAuto.NumarAuto, "BT-00-BBB");
            _autoService.Delete(newAuto.AutoId);
            _sasiuService.Delete(newAuto.Sasiu.SasiuId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityUpdateException))]
        public void Auto_UPDATE_BadExample()
        {
            var entity = _autoService.GetById(1);
            entity.AutoId = 0;
            _autoService.Update(entity);
        }

    }
}
