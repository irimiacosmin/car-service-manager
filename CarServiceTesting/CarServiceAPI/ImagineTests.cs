﻿using System;
using System.Linq;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class ImagineTests
    {
        private readonly ImagineService _imagineService = new ImagineService();

        [TestMethod]
        public void Imagine_GET_GetById_GoodExample()
        {
            var entity = _imagineService.GetById(1);
            Assert.AreEqual(727, entity.DetaliuComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Imagine_GET_GetById_BadExample()
        {
            _imagineService.GetById(0);
        }

        [TestMethod]
        public void Imagine_GET_GetByData_GoodExample()
        {
            var entity =_imagineService.GetByData(new DateTime(2018, 6 ,5, 23,34,58,20));
            Assert.AreEqual(1, entity.Count());

        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Imagine_GET_GetByData_BadExample()
        {
            _imagineService.GetByData(new DateTime(1500,1,1));
        }

        [TestMethod]
        public void Imagine_GET_GetByDetaliiComandaId_GoodExample()
        {
            var entity = _imagineService.GetByDetaliiComandaId(1);
            Assert.AreEqual(2, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Imagine_GET_GetByDetaliiComandaId_BadExample()
        {
            _imagineService.GetByDetaliiComandaId(0);
        }

        [TestMethod]
        public void Imagine_GET_GetByTitlu_GoodExample()
        {
            var entity = _imagineService.GetByTitlu("Foto-5866712405");
            Assert.AreEqual(1, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Imagine_GET_GetByTitlu_BadExample()
        {
            _imagineService.GetByTitlu("0");
        }


        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Imagine_GET_GetByDetaliuComandaId_BadExample()
        {
            _imagineService.GetByDetaliiComandaId(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Imagine_ADD_BadExample()
        {
            _imagineService.Add(_imagineService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Imagine_DELETE_BadExample()
        {
            _imagineService.Delete(0);
        }
    }
}
