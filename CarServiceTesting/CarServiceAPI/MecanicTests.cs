﻿using System;
using System.Linq;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class MecanicTests
    {
        private readonly MecanicService _mecanicService = new MecanicService();

        [TestMethod]
        public void Mecanic_GET_GetById_GoodExample()
        {
            var entity = _mecanicService.GetById(1);
            Assert.AreEqual(727, entity.DetaliuComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Mecanic_GET_GetById_BadExample()
        {
            _mecanicService.GetById(0);
        }

        [TestMethod]
        public void Mecanic_GET_GetByDenumire_GoodExample()
        {
            var entity = _mecanicService.GetByNume("Tomescu");
            Assert.AreEqual(5, entity.Count());

        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Mecanic_GET_GetByDenumire_BadExample()
        {
            _mecanicService.GetByNume("a");
        }

        [TestMethod]
        public void Mecanic_GET_GetByDetaliuComandaId_GoodExample()
        {
            var entity = _mecanicService.GetByDetaliuComandaId(207);
            Assert.AreEqual(1, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Mecanic_GET_GetByDetaliuComandaId_BadExample()
        {
            _mecanicService.GetByDetaliuComandaId(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Mecanic_ADD_BadExample()
        {
            _mecanicService.Add(_mecanicService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Mecanic_DELETE_BadExample()
        {
            _mecanicService.Delete(0);
        }
    }
}
