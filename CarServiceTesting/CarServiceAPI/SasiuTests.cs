﻿using System;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class SasiuTests
    {
        private readonly SasiuService _sasiuService = new SasiuService();

        [TestMethod]
        public void Sasiu_GET_GetById_GoodExample()
        {
            var entity = _sasiuService.GetById(1);
            Assert.AreEqual("Cadillac Allanté", entity.Denumire);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Sasiu_GET_GetById_BadExample()
        {
            _sasiuService.GetById(0);
        }

        [TestMethod]
        public void Sasiu_GET_GetByDenumire_GoodExample()
        {
            var entity = _sasiuService.GetByDenumire("Cadillac Allanté");
            Assert.AreEqual(1, entity.SasiuId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Sasiu_GET_GetByDenumire_BadExample()
        {
            _sasiuService.GetByDenumire("a");
        }

        [TestMethod]
        public void Sasiu_GET_GetByCodSasiu_GoodExample()
        {
            var entity = _sasiuService.GetByCodSasiu("02");
            Assert.AreEqual(1, entity.SasiuId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Sasiu_GET_GetByCodSasiu_BadExample()
        {
            _sasiuService.GetByCodSasiu("a");
        }


        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Sasiu_ADD_BadExample()
        {
            _sasiuService.Add(_sasiuService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Sasiu_DELETE_BadExample()
        {
            _sasiuService.Delete(0);
        }
    }
}
