﻿using System;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class OperatieTests
    {
        private readonly OperatieService _operatieService = new OperatieService();

        [TestMethod]
        public void Operatie_GET_GetById_GoodExample()
        {
            var entity = _operatieService.GetById(1);
            Assert.AreEqual(727, entity.DetaliuComandaDetaliuComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Operatie_GET_GetById_BadExample()
        {
            _operatieService.GetById(0);
        }

        [TestMethod]
        public void Operatie_GET_GetByDenumire_GoodExample()
        {
            var entity = _operatieService.GetByDenumire("Schimbare brate");
            Assert.AreEqual(5, entity.OperatieId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Operatie_GET_GetByDenumire_BadExample()
        {
            _operatieService.GetByDenumire("a");
        }

        [TestMethod]
        public void Operatie_GET_GetByDetaliuComandaId_GoodExample()
        {
            var entity = _operatieService.GetByDetaliuComandaId(727);
            Assert.AreEqual(1, entity.OperatieId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Operatie_GET_GetByDetaliuComandaId_BadExample()
        {
            _operatieService.GetByDetaliuComandaId(0);
        }

    

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Operatie_ADD_BadExample()
        {
            _operatieService.Add(_operatieService.GetById(1));
        }


        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Operatie_DELETE_BadExample()
        {
            _operatieService.Delete(0);
        }
    }
}
