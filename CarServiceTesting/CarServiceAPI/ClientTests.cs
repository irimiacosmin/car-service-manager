﻿using System;
using System.Linq;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class ClientTests
    {
        private readonly ClientService _clientService = new ClientService();

        [TestMethod]
        public void Client_GET_GetById_GoodExample()
        {
            var entity = _clientService.GetById(1);
            Assert.AreEqual("0745 843 596", entity.Telefon);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetById_BadExample()
        {
            _clientService.GetById(0);
        }

        [TestMethod]
        public void Client_GET_GetByEmail_GoodExample()
        {
            var entity = _clientService.GetByEmail("Sava.Francisc7571@yandex.com");
            Assert.AreEqual(4, entity.ClientId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetByEmail_BadExample()
        {
            _clientService.GetByEmail("a");
        }

        [TestMethod]
        public void Client_GET_GetByNumarDeTelefon_GoodExample()
        {
            var entity = _clientService.GetByNumarDeTelefon("0725 748 746");
            Assert.AreEqual(3, entity.ClientId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetByNumarDeTelefon_BadExample()
        {
            _clientService.GetByNumarDeTelefon("TEST");
        }

        [TestMethod]
        public void Client_GET_GetByGetByFullName_GoodExample()
        {
            var entity = _clientService.GetByFullName("Dima", "Denisa");
            Assert.AreEqual(1, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetByFullName_BadExample()
        {
            _clientService.GetByFullName("TEST", "TEST2");
        }
        
        [TestMethod]
        public void Client_GET_GetByNume_GoodExample()
        {
            var entity = _clientService.GetByNume("Cristea");
            Assert.AreEqual(8, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetByNume_BadExample()
        {
            _clientService.GetByNume("TEST");
        }

        [TestMethod]
        public void Client_GET_GetByPrenume_GoodExample()
        {
            var entity = _clientService.GetByPrenume("Sorina");
            Assert.AreEqual(1, entity.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Client_GET_GetByPrenume_BadExample()
        {
            _clientService.GetByPrenume("TEST");
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Client_ADD_BadExample()
        {
            _clientService.Add(_clientService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Client_DELETE_BadExample()
        {
            _clientService.Delete(0);
        }
    }
}
