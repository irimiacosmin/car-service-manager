﻿using System;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class StareComandaTests
    {
        private readonly StareComandaService _stareComandaService = new StareComandaService();

        [TestMethod]
        public void StareComanda_GET_GetById_GoodExample()
        {
            var entity = _stareComandaService.GetById(1);
            Assert.AreEqual("In asteptare", entity.Denumire);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void StareComanda_GET_GetById_BadExample()
        {
            _stareComandaService.GetById(0);
        }


        [TestMethod]
        public void StareComanda_GET_GetByDenumire_GoodExample()
        {
            var entity = _stareComandaService.GetByDenumire("In asteptare");
            Assert.AreEqual(1, entity.StareComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void StareComanda_GET_GetByDenumire_BadExample()
        {
            _stareComandaService.GetByDenumire("a");
        }

        [TestMethod]
        public void StareComanda_ADD_GoodExample()
        {
            var entity = new StareComanda();
            entity.Denumire = "TEST";
            entity.isDeleted = false;
            var newEntity = _stareComandaService.Add(entity);
            Assert.AreEqual(entity.Denumire, newEntity.Denumire);
            _stareComandaService.Delete(newEntity.StareComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void StareComanda_ADD_BadExample()
        {
            _stareComandaService.Add(_stareComandaService.GetById(1));
        }

        [TestMethod]
        public void StareComanda_UPDATE_DELETE_GoodExample()
        {
            var entity = new StareComanda();
            entity.Denumire = "TEST";
            entity.isDeleted = false;
            var newEntity = _stareComandaService.Add(entity);
            newEntity.Denumire = "NotTest";
            _stareComandaService.Update(newEntity);
            Assert.AreEqual("NotTest", newEntity.Denumire);
            _stareComandaService.Delete(newEntity.StareComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void StareComanda_UPDATE_BadExample()
        {
            _stareComandaService.Add(_stareComandaService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void StareComanda_DELETE_BadExample()
        {
            _stareComandaService.Delete(0);
        }
    }
}
