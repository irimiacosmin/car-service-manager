﻿using System;
using System.Linq;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarServiceTesting.CarServiceAPI
{
    [TestClass]
    public class MaterialTests
    {
        private readonly MaterialService _materialService = new MaterialService();

        [TestMethod]
        public void Material_GET_GetById_GoodExample()
        {
            var entity = _materialService.GetById(1);
            Assert.AreEqual(727, entity.DetaliuComandaId);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Material_GET_GetById_BadExample()
        {
            _materialService.GetById(0);
        }


        [TestMethod]
        public void Material_GET_GetByDenumire_GoodExample()
        {
            var entity = _materialService.GetByDenumire("Cutie de viteze");
            Assert.AreEqual(1, entity.MaterialId);

        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Material_GET_GetByDenumire_BadExample()
        {
            _materialService.GetByDenumire("a");
        }

        [TestMethod]
        public void Material_GET_GetByDetaliuComandaId_GoodExample()
        {
            var entity = _materialService.GetByDetaliiComandaId(727);
            Assert.AreEqual(2, entity.Count());

        }

        [TestMethod]
        [ExpectedException(typeof(EntityGetException))]
        public void Material_GET_GetByDetaliuComandaId_BadExample()
        {
            _materialService.GetByDetaliiComandaId(0);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityAddException))]
        public void Material_ADD_BadExample()
        {
            _materialService.Add(_materialService.GetById(1));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityDeleteException))]
        public void Material_DELETE_BadExample()
        {
            _materialService.Delete(0);
        }
    }
}
