﻿using CarServiceWPF.CarServiceWCFReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarServiceWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarServiceAPIWCFClient _wcfReference;

        private Client currentClient = null;
        private Auto currentAuto = null;
        private Comanda currentComanda = null;

        private bool _focusOnClient = true;
        private bool _focusOnAuto = false;
        private bool _focusOnComanda = false;

        public MainWindow()
        {
            InitializeComponent();
            _wcfReference = new CarServiceAPIWCFClient();
            tab_client.Visibility = Visibility.Hidden;
            tab_auto.Visibility = Visibility.Hidden;
            tab_comanda.Visibility = Visibility.Hidden;
            tab_client_m.Visibility = Visibility.Hidden;
            tab_auto_m.Visibility = Visibility.Hidden;
            tab_comanda_m.Visibility = Visibility.Hidden;
            tab_imagini.Visibility = Visibility.Hidden;
            tab_materiale.Visibility = Visibility.Hidden;
            
            RePopulateClientList(null);
            dg_clienti.SelectedIndex = 0;
        }

        private void RePopulateClientList(String startsWith)
        {
            dg_clienti.ItemsSource = new List<String>();
            List<Client> listForPopulate;
            if (startsWith == null)
            {
                _wcfReference = new CarServiceAPIWCFClient();
                listForPopulate = _wcfReference.GetAllClients().ToList();
            }
            else
            {
                listForPopulate = _wcfReference.GetAllClients().Where(x =>
                    x.Nume.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.Prenume.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.ClientId.ToString().ToLower().StartsWith(startsWith.ToLower())).ToList();
            }
            if (listForPopulate.Count == 0)
            {
                btn_detaliiClient.IsEnabled = false;
                btn_addAuto.IsEnabled = false;
                btn_detaliiAuto.IsEnabled = false;
                btn_addComanda.IsEnabled = false;
                btn_detaliiComanda.IsEnabled = false;
            }
            else
            {
                ReFillAutoList(listForPopulate.ElementAt(0).ClientId);
                dg_clienti.ItemsSource = listForPopulate;
                btn_detaliiClient.IsEnabled = true;
           
            }
        }

        private void RePopulateAutoList(String startsWith)
        {
            dg_auto.ItemsSource = new List<String>();
            List<Auto> listForPopulate;
            if (startsWith == null)
            {
                listForPopulate = _wcfReference.GetAllAutos().ToList();
            }
            else
            {
                listForPopulate = _wcfReference.GetAllAutos().Where(x =>
                    x.NumarAuto.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.SerieSasiu.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.AutoId.ToString().ToLower().StartsWith(startsWith.ToLower())).ToList();
            }
            if (listForPopulate.Count == 0)
            {
                btn_detaliiClient.IsEnabled = false;
                btn_detaliiAuto.IsEnabled = false;
                btn_detaliiComanda.IsEnabled = false;
            }
            else
            {
                btn_detaliiAuto.IsEnabled = true;
                ReFillClientList(listForPopulate.ElementAt(0).AutoId);
                dg_auto.ItemsSource = listForPopulate;
            }       
        }

        private void ReFillClientList(int autoId)
        {
            try
            {
                var client = _wcfReference.GetClientByAutoId(autoId);
                dg_clienti.Items.Add(client);
                btn_detaliiClient.IsEnabled = true;
            }
            catch (Exception)
            {
                btn_detaliiClient.IsEnabled = false;
            }
        }

        private void ReFillAutoList(int clientId)
        {
            try
            {
                var listAuto = _wcfReference.GetAllAutosByClientId(clientId);
                if (_focusOnClient)
                    ReFillComenziList(listAuto.ElementAt(0).AutoId);
                dg_auto.ItemsSource = listAuto;
                btn_addAuto.IsEnabled = true;
                btn_detaliiAuto.IsEnabled = true;
                dg_auto.SelectedIndex = 0;
            }
            catch (Exception)
            {
                btn_detaliiAuto.IsEnabled = false;
                btn_addComanda.IsEnabled = false;
                btn_detaliiComanda.IsEnabled = false;
            }  
        }

        private void ReFillComenziList(int autoId)
        {
            try
            {
                var listComanda = _wcfReference.GetAllComandasByAutoId(autoId).ToList();
                dg_comenzi.ItemsSource = listComanda;
                btn_addComanda.IsEnabled = true;
                btn_detaliiComanda.IsEnabled = true;
                dg_comenzi.SelectedIndex = 0;
            }
            catch (Exception)
            {
                btn_detaliiComanda.IsEnabled = false;
            }
        }

        private int GetFocus()
        {
            if (_focusOnClient)
            {
                return 1;
            }
            else if (_focusOnAuto)
            {
                return 2;
            }
            else if (_focusOnComanda)
            {
                return 3;
            }

            return 0;
        }

        private void SetFocus(bool focusOnClient, bool focusOnAuto, bool focusOnComanda)
        {
            this._focusOnClient = focusOnClient;
            this._focusOnAuto = focusOnAuto;
            this._focusOnComanda = focusOnComanda;
        }

        private void Btn_addClient_Click(object sender, RoutedEventArgs e)
        {
            tab_client.Visibility = Visibility.Visible;
            tab_client.Focus();

        }

        private void Btn_detaliiClient_Click(object sender, RoutedEventArgs e)
        {
            tab_client_m.Visibility = Visibility.Visible;
            tab_client_m.Focus();

            currentClient = (Client)dg_clienti.SelectedCells[0].Item;
            tb_client_nume_m.Text = currentClient.Nume;
            tb_client_prenume_m.Text = currentClient.Prenume;
            tb_client_adresa_m.Text = currentClient.Adresa;
            tb_client_localitate_m.Text = currentClient.Localitate;
            tb_client_judet_m.Text = currentClient.Judet;
            tb_client_telefon_m.Text = currentClient.Telefon;
            tb_client_email_m.Text = currentClient.Email;
        }

        private void Tb_cautaClient_TextChanged(object sender, TextChangedEventArgs e)
        {
            tb_cautaAuto.Text = "";
            SetFocus(true, false, false);
            RePopulateClientList(tb_cautaClient.Text);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Tb_cautaAuto_TextChanged(object sender, TextChangedEventArgs e)
        {
            tb_cautaClient.Text = "";
            SetFocus(false, true, false);
            RePopulateAutoList(tb_cautaAuto.Text);
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void Dg_auto_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var autoId = ((Auto)dg_auto.SelectedCells[0].Item).AutoId;

                switch (GetFocus())
                {
                    case 1:
                        {
                            ReFillComenziList(autoId);
                            break;
                        }
                    case 2:
                        {
                            ReFillClientList(autoId);
                            ReFillComenziList(autoId);
                            break;
                        }
                    case 3:
                        {
                            break;
                        }
                    default: break;
                }
            }
            catch (Exception) { }
        }

        private void Dg_comenzi_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedIndex = dg_comenzi.SelectedIndex;
            switch (GetFocus())
            {
                case 1:
                    {
                        break;
                    }
                case 2:
                    {
                        break;
                    }
                case 3:
                    {
                        ReFillClientList(selectedIndex);
                        ReFillAutoList(selectedIndex);
                        break;
                    }
                default: break;
            }
        }

        private void Btn_addAuto_Click(object sender, RoutedEventArgs e)
        {
            tab_auto.Visibility = Visibility.Visible;
            tab_auto.Focus();

            currentClient = (Client)dg_clienti.SelectedCells[0].Item;

            lbl_auto_id_client.Content = currentClient.ClientId;
            lbl_auto_nume.Content = currentClient.Nume;
            lbl_auto_prenume.Content = currentClient.Prenume;
        }

        private void Btn_detaliiAuto_Click(object sender, RoutedEventArgs e)
        {
            tab_auto_m.Visibility = Visibility.Visible;
            tab_auto_m.Focus();

            currentClient = (Client)dg_clienti.SelectedCells[0].Item;
            currentAuto = (Auto)dg_auto.SelectedCells[0].Item;
            lbl_auto_id_client_m.Content = currentClient.ClientId;
            lbl_auto_nume_m.Content = currentClient.Nume;
            lbl_auto_prenume_m.Content = currentClient.Prenume;
            tb_auto_nr_inmatriculare_m.Text = currentAuto.NumarAuto;
            tb_auto_serie_sasiu_m.Text = currentAuto.SerieSasiu;       
        }

        private void Btn_addComanda_Click(object sender, RoutedEventArgs e)
        {
            tab_comanda.Visibility = Visibility.Visible;
            tab_comanda.Focus();

            currentClient = (Client)dg_clienti.SelectedCells[0].Item;
            currentAuto = (Auto)dg_auto.SelectedCells[0].Item;

            lbl_comanda_id_auto.Content = currentAuto.AutoId;
            lbl_comanda_nr_inmatriculare.Content = currentAuto.NumarAuto;
            lbl_comanda_serie_sasiu.Content = currentAuto.SerieSasiu;
        
            lbl_comanda_id_client.Content = currentClient.ClientId;
            lbl_comanda_nume.Content = currentClient.Nume;
            lbl_comanda_prenume.Content = currentClient.Prenume;     
        }

        private void Btn_detaliiComanda_Click(object sender, RoutedEventArgs e)
        {
            tab_comanda_m.Visibility = Visibility.Visible;
            tab_comanda_m.Focus();


            currentClient = (Client)dg_clienti.SelectedCells[0].Item;
            currentAuto = (Auto)dg_auto.SelectedCells[0].Item;
            currentComanda = (Comanda)dg_comenzi.SelectedCells[0].Item;

            lbl_comanda_id_auto_m.Content = currentAuto.AutoId;
            lbl_comanda_nr_inmatriculare_m.Content = currentAuto.NumarAuto;
            lbl_comanda_serie_sasiu_m.Content = currentAuto.SerieSasiu;

            lbl_comanda_id_client_m.Content = currentClient.ClientId;
            lbl_comanda_nume_m.Content = currentClient.Nume;
            lbl_comanda_prenume_m.Content = currentClient.Prenume;

            tb_comanda_km_bord_m.Text = currentComanda.KmBord.ToString();
            tb_comanda_descriere_problema_m.Text = currentComanda.Descriere;
            dt_comanda.SelectedDate = currentComanda.DataSystem;

            var detaliuComanda = _wcfReference.GetDetaliuComandaById(currentComanda.ComandaId);
            var len = detaliuComanda.Operaties.Length;
            dg_comanda_operatii.ItemsSource = detaliuComanda.Operaties;
        }

        private void Btn_client_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_client.Visibility = Visibility.Hidden;
            tab_service.Focus();
        }

        private void Btn_auto_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_auto.Visibility = Visibility.Hidden;
            tab_service.Focus();

        }

        private void Btn_comanda_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_comanda.Visibility = Visibility.Hidden;
            tab_service.Focus();

        }

        private void Btn_client_m_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_client_m.Visibility = Visibility.Hidden;
            tab_service.Focus();

        }

        private void Btn_auto_m_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_auto_m.Visibility = Visibility.Hidden;
            tab_service.Focus();
        }

        private void Btn_comanda_m_page_exit_Click(object sender, RoutedEventArgs e)
        {
            tab_comanda_m.Visibility = Visibility.Hidden;
            tab_service.Focus();
        }

        private void Btn_client_add_Click(object sender, RoutedEventArgs e)
        {
            var newClient = new Client();
            newClient.Nume = tb_client_nume.Text;
            newClient.Prenume = tb_client_prenume.Text;
            newClient.Adresa = tb_client_adresa.Text;
            newClient.Localitate = tb_client_localitate.Text;
            newClient.Judet = tb_client_judet.Text;
            newClient.Telefon = tb_client_telefon.Text;
            newClient.Email = tb_client_email.Text;
            newClient.isDeleted = false;

            if (Utils.isValidClient(newClient))
            {
                
                try
                {
                    _wcfReference.AddClient(newClient);
                    MessageBox.Show("Clientul a fost adaugat cu succes!");
                    tab_client.Visibility = Visibility.Hidden;
                    tab_service.Focus();

                }
                catch (Exception)
                {
                    MessageBox.Show("Clientul nu a putut fi adaugat!");
                }
            }
            tab_client.Visibility = Visibility.Hidden;
        }

        private void Btn_client_update_Click(object sender, RoutedEventArgs e)
        {
            var newClient = currentClient;
            newClient.Nume = tb_client_nume.Text;
            newClient.Prenume = tb_client_prenume.Text;
            newClient.Adresa = tb_client_adresa.Text;
            newClient.Localitate = tb_client_localitate.Text;
            newClient.Judet = tb_client_judet.Text;
            newClient.Telefon = tb_client_telefon.Text;
            newClient.Email = tb_client_email.Text;
            newClient.isDeleted = false;

            if (Utils.isValidClient(newClient))
            {
                try
                {
                    _wcfReference.UpdateClient(newClient);
                    MessageBox.Show("Clientul a fost editat cu succes!");
                    tab_client_m.Visibility = Visibility.Hidden;
                    RePopulateClientList(null);
                    tab_service.Focus();
                }
                catch (Exception)
                {
                    MessageBox.Show("Clientul nu a putut fi editat!");
                }
            }

        }

        private void Btn_client_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _wcfReference.DeleteClient(currentClient.ClientId);
                MessageBox.Show("Clientul a fost sters cu succes!");
                tab_client_m.Visibility = Visibility.Hidden;
                tab_service.Focus();
                RePopulateClientList(null);

            }
            catch (Exception)
            {
                MessageBox.Show("Clientul nu a putut fi sters!");
            }
        }

        private void btn_auto_update_Click(object sender, RoutedEventArgs e)
        {
            var newAuto = currentAuto;
            newAuto.NumarAuto = tb_auto_nr_inmatriculare.Text;
            newAuto.SerieSasiu = tb_auto_serie_sasiu.Text;
            try
            {
                newAuto.Sasiu.CodSasiu = tb_auto_cod_sasiu.Text;
                newAuto.Sasiu.Denumire = tb_auto_denumire.Text;
            }
            catch (Exception) { }
            
            if (Utils.isValidAuto(newAuto))
            {
                try
                {
                    _wcfReference.UpdateAuto(newAuto);
                    MessageBox.Show("Autoturismul a fost editat cu succes!");
                    tab_auto_m.Visibility = Visibility.Hidden;
                    RePopulateAutoList(null);
                    RePopulateClientList(null);
                    tab_service.Focus();
                }
                catch (Exception)
                {
                    MessageBox.Show("Autoturismul nu a putut fi editat!");
                }
            }
        }

        private void btn_auto_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _wcfReference.DeleteAuto(currentAuto.AutoId);
                MessageBox.Show("Autoturismul a fost sters cu succes!");
                tab_auto_m.Visibility = Visibility.Hidden;
                RePopulateAutoList(null);
                RePopulateClientList(null);
                tab_service.Focus();

            }
            catch (Exception)
            {
                MessageBox.Show("Autoturismul nu a putut fi sters!");
            }
        }

        private void btn_auto_add_Click(object sender, RoutedEventArgs e)
        {
            var newAuto = new Auto();
            newAuto.NumarAuto = tb_auto_nr_inmatriculare.Text;
            newAuto.SerieSasiu = tb_auto_serie_sasiu.Text;

            var newSasiu = new Sasiu();
            newSasiu.CodSasiu = tb_auto_cod_sasiu.Text;
            newSasiu.Denumire = tb_auto_denumire.Text;
            newSasiu.isDeleted = false;

            newAuto.Sasiu = newSasiu;
            newAuto.ClientId = currentClient.ClientId;
            newAuto.isDeleted = false;
            if (Utils.isValidAuto(newAuto))
            {
                try
                {
                    _wcfReference.AddAuto(newAuto);
                    MessageBox.Show("Autoturismul a fost adaugat cu succes!");
                    tab_auto.Visibility = Visibility.Hidden;
                    tab_service.Focus();
                }
                catch (Exception)
                {
                    MessageBox.Show("Autoturismul nu a putut fi adaugat!");
                }
            }
        }

        private void btn_comanda_add_Click(object sender, RoutedEventArgs e)
        {
            var newComanda = new Comanda();
            var kmVal = -1;
            try
            {
                kmVal = Int32.Parse(tb_comanda_km_bord.Text);
            }
            catch (Exception) { }
            DateTime myDateTime = DateTime.Now;
            newComanda.KmBord = kmVal;
            newComanda.DataSystem = myDateTime;
            newComanda.DataProgramare = dtp_comanda.SelectedDate.Value.Date;
            newComanda.DataFinalizare = new DateTime(2050, 01, 01);
            newComanda.Descriere = tb_comanda_descriere_problema.Text;
            newComanda.ValoarePiese = new decimal(0);
            newComanda.StareComanda = _wcfReference.GetStareComandaById(1);      
            newComanda.Auto = currentAuto;
            newComanda.Client = currentClient;
            newComanda.isDeleted = false;
            try
            {
                _wcfReference.AddComanda(newComanda);
                tab_service.Focus();
                tab_comanda.Visibility = Visibility.Hidden;
                MessageBox.Show("Comanda a fost adaugat cu succes!");
            }
            catch (Exception)
            {
                MessageBox.Show("Comanda nu a putut fi adaugat!");
            }
        }

        private void btn_comanda_save_Click(object sender, RoutedEventArgs e)
        {
            var newComanda = currentComanda;
            try
            {
                newComanda.KmBord = int.Parse(tb_comanda_km_bord_m.Text);
                newComanda.Descriere = tb_comanda_descriere_problema_m.Text;
                newComanda.DataProgramare = dt_comanda.SelectedDate.Value.Date;
                _wcfReference.UpdateComanda(newComanda);
            }    
            catch (Exception)
            {
                MessageBox.Show("Eroare la editarea comenzii.");
            }
        }

        private void btn_comanda_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _wcfReference.DeleteComanda(currentComanda.ComandaId);
                MessageBox.Show("Comanda a fost stersa cu succes!");
                tab_comanda_m.Visibility = Visibility.Hidden;
                tab_service.Focus();
                RePopulateAutoList(null);
                RePopulateClientList(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Comanda nu a putut fi stearsa!");
            }
        }

        private void btn_comanda_finish_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var newComanda = currentComanda;
                newComanda.StareComanda = _wcfReference.GetStareComandaById(2);
                newComanda.DataFinalizare = DateTime.Now;
                _wcfReference.UpdateComanda(newComanda);
                MessageBox.Show("Comanda a fost finalizata cu succes!");
                tab_comanda_m.Visibility = Visibility.Hidden;
                tab_service.Focus();
                RePopulateAutoList(null);
                RePopulateClientList(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Comanda nu a putut fi finalizata!");
            }
        }

        private void btn_comanda_refuz_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var newComanda = currentComanda;
                newComanda.StareComanda = _wcfReference.GetStareComandaById(3);
                newComanda.DataFinalizare = DateTime.Now;
                _wcfReference.UpdateComanda(newComanda);
                MessageBox.Show("Comanda a fost refuzata cu succes!");
                tab_comanda_m.Visibility = Visibility.Hidden;
                tab_service.Focus();
                RePopulateAutoList(null);
                RePopulateClientList(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Comanda nu a putut fi refuzata!");
            }
        }

        private void Dg_client_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var clientId = ((Client)dg_clienti.SelectedCells[0].Item).ClientId;
                switch (GetFocus())
                {
                    case 1:
                        {
                            ReFillAutoList(clientId);
                            break;
                        }
                    default: break;
                }
            }
            catch (Exception) { }
        }
    }
}
