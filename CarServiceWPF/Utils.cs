﻿using CarServiceWPF.CarServiceWCFReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace CarServiceWPF
{
    public class Utils
    {

        public static bool isValidClient(Client client)
        {
            return IsValidName(client.Nume, "Numele nu este valid.") && 
                   IsValidName(client.Prenume, "Prenumele nu este valid.") &&
                   IsValidSomething(client.Adresa, "Adresa nu este valida.") &&
                   IsValidSomething(client.Localitate, "Localitatea nu este valida.") &&
                   IsValidJudet(client.Judet, "Judetul nu este valid.") &&
                   IsValidPhone(client.Telefon) &&
                   IsValidEmail(client.Email) ;
        }

        public static bool isValidAuto(Auto auto)
        {
            try
            {
                return IsValidNumarDeInmatriculare(auto.NumarAuto) &&
                       IsValidSerieSasiu(auto.SerieSasiu) &&
                       IsValidCodSasiu(auto.Sasiu.CodSasiu) &&
                       IsValidDenumire(auto.Sasiu.Denumire);
            }
            catch (Exception)
            {
               
                return IsValidNumarDeInmatriculare(auto.NumarAuto) &&
                       IsValidSerieSasiu(auto.SerieSasiu);
            }
           
        }

        private static bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Email-ul nu este valid.");
                return false;
            }
        }

        private static bool IsValidName(string name, string error)
        {
            Regex regex = new Regex(@"[A-Z][a-z]*((-|\s)[A-Z][a-z]*)*");
            Match match = regex.Match(name);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show(error);
            return false;
        }

        private static bool IsValidPhone(string phone)
        {
            Regex regex = new Regex(@"[0-9]{9,12}");
            Match match = regex.Match(phone);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show("Numarul de telefon nu este valid.");
            return false;
        }

        private static bool IsValidJudet(string judet, string error)
        {
            Regex regex = new Regex(@"[A-Z][a-z]{4,12}((-|\s)[A-Z][a-z]*)*");
            Match match = regex.Match(judet);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show(error);
            return false;
        }

        private static bool IsValidSomething(string ceva, string error)
        {
            Regex regex = new Regex(@"[a-zA-Z0-9\s\.-]{4,25}");
            Match match = regex.Match(ceva);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show(error);
            return false;
        }

        private static bool IsValidNumarDeInmatriculare(string nr)
        {
            Regex regex = new Regex(@"[A-Za-z]{2}(\s|-)*[0-9]{2}(\s|-)*[A-Za-z]{3}");
            Match match = regex.Match(nr);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show("Numarul de inmatriculare nu este valid.");
            return false;
        }

        private static bool IsValidSerieSasiu(string serie)
        {
            Regex regex = new Regex(@"[A-Za-z]{6}[A-Za-z0-9]{8,14}");
            Match match = regex.Match(serie);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show("Seria de sasiu nu este valida.");
            return false;
        }

        private static bool IsValidCodSasiu(string cod)
        {
            Regex regex = new Regex(@"[A-Za-z0-9]{2}");
            Match match = regex.Match(cod);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show("Codul de sasiu nu este valid.");
            return false;
        }

        private static bool IsValidDenumire(string denumire)
        {
            Regex regex = new Regex(@"[A-Za-z0-9]{5,50}");
            Match match = regex.Match(denumire);
            if (match.Success)
            {
                return true;
            }
            MessageBox.Show("Denumirea nu este valida.");
            return false;
        }
    }
}
