﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IComandaInterface
    {
        [OperationContract]
        Comanda AddComanda(Comanda comanda);

        [OperationContract]
        List<Comanda> GetAllComandas();

        [OperationContract]
        List<Comanda> GetAllComandasByAutoId(int id);

        [OperationContract]
        Comanda GetComandaById(int id);

        [OperationContract]
        void UpdateComanda(Comanda comanda);

        [OperationContract]
        void DeleteComanda(int id);
    }
}
