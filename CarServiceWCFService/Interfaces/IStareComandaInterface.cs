﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IStareComandaInterface
    {
        [OperationContract]
        StareComanda AddStareComanda(StareComanda stareComanda);

        [OperationContract]
        List<StareComanda> GetAllStareComandas();

        [OperationContract]
        StareComanda GetStareComandaById(int id);

        [OperationContract]
        void UpdateStareComanda(StareComanda stareComanda);

        [OperationContract]
        void DeleteStareComanda(int id);
    }
}
