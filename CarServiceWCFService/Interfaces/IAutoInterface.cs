﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IAutoInterface
    {
        [OperationContract]
        Auto AddAuto(Auto auto);

        [OperationContract]
        IList<Auto> GetAllAutos();

        [OperationContract]
        List<Auto> GetAllAutosByClientId(int id);

        [OperationContract]
        Auto GetAutoById(int id);

        [OperationContract]
        void UpdateAuto(Auto auto);

        [OperationContract]
        void DeleteAuto(int id);
    }
}
