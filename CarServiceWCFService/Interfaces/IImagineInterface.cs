﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IImagineInterface
    {
        [OperationContract]
        Imagine AddImagine(Imagine imagine);

        [OperationContract]
        List<Imagine> GetAllImagines();

        [OperationContract]
        Imagine GetImagineById(int id);

        [OperationContract]
        void UpdateImagine(Imagine imagine);

        [OperationContract]
        void DeleteImagine(int id);
    }
}
