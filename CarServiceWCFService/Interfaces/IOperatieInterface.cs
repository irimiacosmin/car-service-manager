﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IOperatieInterface
    {
        [OperationContract]
        Operatie AddOperatie(Operatie operatie);

        [OperationContract]
        List<Operatie> GetAllOperaties();

        [OperationContract]
        Operatie GetOperatieById(int id);

        [OperationContract]
        void UpdateOperatie(Operatie operatie);

        [OperationContract]
        void DeleteOperatie(int id);
    }
}
