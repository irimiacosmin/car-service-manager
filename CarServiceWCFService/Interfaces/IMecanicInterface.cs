﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IMecanicInterface
    {
        [OperationContract]
        Mecanic AddMecanic(Mecanic mecanic);

        [OperationContract]
        List<Mecanic> GetAllMecanics();

        [OperationContract]
        Mecanic GetMecanicById(int id);

        [OperationContract]
        void UpdateMecanic(Mecanic mecanic);

        [OperationContract]
        void DeleteMecanic(int id);
    }
}
