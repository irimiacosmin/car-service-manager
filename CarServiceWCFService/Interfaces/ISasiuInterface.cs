﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface ISasiuInterface
    {
        [OperationContract]
        Sasiu AddSasiu(Sasiu sasiu);

        [OperationContract]
        List<Sasiu> GetAllSasius();

        [OperationContract]
        Sasiu GetSasiuById(int id);

        [OperationContract]
        void UpdateSasiu(Sasiu sasiu);

        [OperationContract]
        void DeleteSasiu(int id);
    }
}
