﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IDetaliuComandaInterface
    {
        [OperationContract]
        DetaliuComanda AddDetaliuComanda(DetaliuComanda DetaliuComanda);

        [OperationContract]
        List<DetaliuComanda> GetAllDetaliuComandas();

        [OperationContract]
        DetaliuComanda GetDetaliuComandaById(int id);

        [OperationContract]
        void UpdateDetaliuComanda(DetaliuComanda DetaliuComanda);

        [OperationContract]
        void DeleteDetaliuComanda(int id);
    }
}
