﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IClientInterface
    {
        [OperationContract]
        Client AddClient(Client client);

        [OperationContract]
        List<Client> GetAllClients();

        [OperationContract]
        Client GetClientById(int id);

        [OperationContract]
        Client GetClientByAutoId(int id);

        [OperationContract]
        void UpdateClient(Client client);

        [OperationContract]
        void DeleteClient(int id);
    }
}
