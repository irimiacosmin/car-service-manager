﻿using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceWCFService.Interfaces
{
    [ServiceContract]
    public interface IMaterialInterface
    {
        [OperationContract]
        Material AddMaterial(Material material);

        [OperationContract]
        List<Material> GetAllMaterials();

        [OperationContract]
        Material GetMaterialById(int id);

        [OperationContract]
        void UpdateMaterial(Material material);

        [OperationContract]
        void DeleteMaterial(int id);
    }
}
