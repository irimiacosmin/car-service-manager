﻿using CarServiceWCFService.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CarServiceWCFService
{
    [ServiceContract]
    public interface ICarServiceAPIWCF : IClientInterface, IAutoInterface, IComandaInterface, 
                                            IDetaliuComandaInterface, IImagineInterface, IMaterialInterface, 
                                            IMecanicInterface, IOperatieInterface, ISasiuInterface, IStareComandaInterface
    {
    }
}

