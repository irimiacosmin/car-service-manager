﻿using CarServiceAPI.Services;
using CarServiceWCFService.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CarServiceWCFService
{
    public class CarServiceAPIWCF : ICarServiceAPIWCF
    {
        private static Model1Container _dbContext = new Model1Container();
        private AutoService _autoService = new AutoService(_dbContext);
        private ClientService _clientService = new ClientService(_dbContext);
        private ComandaService _comandaService = new ComandaService(_dbContext);
        private DetaliiComandaService _detaliiComandaService = new DetaliiComandaService(_dbContext);
        private ImagineService _imagineService = new ImagineService(_dbContext);
        private MaterialService _materialService = new MaterialService(_dbContext);
        private MecanicService _mecanicService = new MecanicService(_dbContext);
        private OperatieService _operatieService = new OperatieService(_dbContext);
        private SasiuService _sasiuService = new SasiuService(_dbContext);
        private StareComandaService _stareComandaService = new StareComandaService(_dbContext);

        public Auto AddAuto(Auto auto)
        {
            return _autoService.Add(auto);
        }

        public Client AddClient(Client client)
        {
            return _clientService.Add(client);
        }

        public Comanda AddComanda(Comanda comanda)
        {
            return _comandaService.Add(comanda);
        }

        public DetaliuComanda AddDetaliuComanda(DetaliuComanda DetaliuComanda)
        {
            return _detaliiComandaService.Add(DetaliuComanda);
        }

        public Imagine AddImagine(Imagine imagine)
        {
            return _imagineService.Add(imagine);
        }

        public Material AddMaterial(Material material)
        {
            return _materialService.Add(material);
        }

        public Mecanic AddMecanic(Mecanic mecanic)
        {
            return _mecanicService.Add(mecanic);
        }

        public Operatie AddOperatie(Operatie operatie)
        {
            return _operatieService.Add(operatie);
        }

        public Sasiu AddSasiu(Sasiu sasiu)
        {
            return _sasiuService.Add(sasiu);
        }

        public StareComanda AddStareComanda(StareComanda stareComanda)
        {
            return _stareComandaService.Add(stareComanda);
        }

        public void DeleteAuto(int id)
        {
            _autoService.Delete(id);
        }

        public void DeleteClient(int id)
        {
            _clientService.Delete(id);
        }

        public void DeleteComanda(int id)
        {
            _comandaService.Delete(id);
        }

        public void DeleteDetaliuComanda(int id)
        {
            _detaliiComandaService.Delete(id);
        }

        public void DeleteImagine(int id)
        {
            _imagineService.Delete(id);
        }

        public void DeleteMaterial(int id)
        {
            _materialService.Delete(id);
        }

        public void DeleteMecanic(int id)
        {
            _mecanicService.Delete(id);
        }

        public void DeleteOperatie(int id)
        {
            _operatieService.Delete(id);
        }

        public void DeleteSasiu(int id)
        {
            _sasiuService.Delete(id);
        }

        public void DeleteStareComanda(int id)
        {
            _stareComandaService.Delete(id);
        }

        public IList<Auto> GetAllAutos()
        {
            return _autoService.GetAll().ToList() ;  
        }

        public List<Auto> GetAllAutosByClientId(int id)
        {
            return _autoService.GetByClientId(id).ToList();     
        }

        public List<Client> GetAllClients()
        {
            return _clientService.GetAll().ToList();
        }

        public List<Comanda> GetAllComandas()
        {
            return _comandaService.GetAll().ToList();
        }

        public List<Comanda> GetAllComandasByAutoId(int id)
        {
            return _comandaService.GetByAutoId(id).ToList();
        }

        public List<DetaliuComanda> GetAllDetaliuComandas()
        {
            return _detaliiComandaService.GetAll().ToList();
        }

        public List<Imagine> GetAllImagines()
        {
            return _imagineService.GetAll().ToList();
        }

        public List<Material> GetAllMaterials()
        {
            return _materialService.GetAll().ToList();
        }

        public List<Mecanic> GetAllMecanics()
        {
            return _mecanicService.GetAll().ToList();
        }

        public List<Operatie> GetAllOperaties()
        {
            return _operatieService.GetAll().ToList();
        }

        public List<Sasiu> GetAllSasius()
        {
            return _sasiuService.GetAll().ToList();
        }

        public List<StareComanda> GetAllStareComandas()
        {
            return _stareComandaService.GetAll().ToList();
        }

        public Auto GetAutoById(int id)
        {
            return _autoService.GetById(id);
        }

        public Client GetClientByAutoId(int id)
        {
            return _autoService.GetById(id).Client;
        }

        public Client GetClientById(int id)
        {
            return _clientService.GetById(id);
        }

        public Comanda GetComandaById(int id)
        {
            return _comandaService.GetById(id);
        }

        public DetaliuComanda GetDetaliuComandaById(int id)
        {
            return _detaliiComandaService.GetById(id);
        }

        public Imagine GetImagineById(int id)
        {
            return _imagineService.GetById(id);
        }

        public Material GetMaterialById(int id)
        {
            return _materialService.GetById(id);
        }

        public Mecanic GetMecanicById(int id)
        {
            return _mecanicService.GetById(id);
        }

        public Operatie GetOperatieById(int id)
        {
            return _operatieService.GetById(id);
        }

        public Sasiu GetSasiuById(int id)
        {
            return _sasiuService.GetById(id);
        }

        public StareComanda GetStareComandaById(int id)
        {
            return _stareComandaService.GetById(id);
        }

        public void UpdateAuto(Auto auto)
        {
            _autoService.Update(auto);
        }

        public void UpdateClient(Client client)
        {
            _clientService.Update(client);
        }

        public void UpdateComanda(Comanda comanda)
        {
            _comandaService.Update(comanda);
        }

        public void UpdateDetaliuComanda(DetaliuComanda DetaliuComanda)
        {
            _detaliiComandaService.Update(DetaliuComanda);
        }

        public void UpdateImagine(Imagine imagine)
        {
            _imagineService.Update(imagine);
        }

        public void UpdateMaterial(Material material)
        {
            _materialService.Update(material);
        }

        public void UpdateMecanic(Mecanic mecanic)
        {
            _mecanicService.Update(mecanic);
        }

        public void UpdateOperatie(Operatie operatie)
        {
            _operatieService.Update(operatie);
        }

        public void UpdateSasiu(Sasiu sasiu)
        {
            _sasiuService.Update(sasiu);
        }

        public void UpdateStareComanda(StareComanda stareComanda)
        {
            _stareComandaService.Update(stareComanda);
        }
    }
}
