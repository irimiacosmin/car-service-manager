﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class StareComandaService : ServiceInterface<StareComanda>
    {
        readonly StareComandaRepository _repository;


        public StareComandaService(Model1Container dbContext)
        {
            this._repository = new StareComandaRepository(dbContext);
        }

        public StareComanda Add(StareComanda entity)
        {
            if (_repository.GetById(entity.StareComandaId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<StareComanda> GetAll()
        {
            IEnumerable<StareComanda> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public StareComanda GetById(int id)
        {
            StareComanda stareComanda = _repository.GetById(id);
            if (stareComanda == null)
            {
                throw new EntityGetException();
            }
            return stareComanda;
        }

        public void Update(StareComanda entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public StareComanda GetByDenumire(string denumire)
        {
            var entity = GetAll().SingleOrDefault(x => x.Denumire.ToLower().Equals(denumire.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }
    }
}
