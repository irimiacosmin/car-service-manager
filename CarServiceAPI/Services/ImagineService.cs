﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class ImagineService : ServiceInterface<Imagine>
    {
        readonly ImagineRepository _repository;

        public ImagineService(Model1Container dbContext)
        {
            this._repository = new ImagineRepository(dbContext);
        }

        public Imagine Add(Imagine entity)
        {
            if (_repository.GetById(entity.ImagineId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Imagine> GetAll()
        {
            IEnumerable<Imagine> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public Imagine GetById(int id)
        {
            Imagine imagine = _repository.GetById(id);
            if (imagine == null)
            {
                throw new EntityGetException();
            }
            return imagine;
        }

        public void Update(Imagine entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public IEnumerable<Imagine> GetByTitlu(string titlu)
        {
            IEnumerable<Imagine> list = GetAll().Where(x => x.Titlu.ToLower().Equals(titlu.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Imagine> GetByDetaliiComandaId(int detaliiComandaId)
        {
            IEnumerable<Imagine> list = GetAll().Where(x => x.DetaliuComanda.DetaliuComandaId == detaliiComandaId);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Imagine> GetByData(DateTime data)
        {
            IEnumerable<Imagine> list = GetAll().Where(x => x.Data.Equals(data));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }
    }
}
