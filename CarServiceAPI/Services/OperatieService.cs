﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class OperatieService : ServiceInterface<Operatie>
    {
        readonly OperatieRepository _repository;

        public OperatieService(Model1Container dbContext)
        {
            this._repository = new OperatieRepository(dbContext);
        }

        public Operatie Add(Operatie entity)
        {
            if (_repository.GetById(entity.OperatieId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Operatie> GetAll()
        {
            IEnumerable<Operatie> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public Operatie GetById(int id)
        {
            Operatie operatie = _repository.GetById(id);
            if (operatie == null)
            {
                throw new EntityGetException();
            }
            return operatie;
        }

        public void Update(Operatie entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public Operatie GetByDenumire(string denumire)
        {
            var entity = GetAll().FirstOrDefault(x => x.Denumire.ToLower().Equals(denumire.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }

        }

        public Operatie GetByDetaliuComandaId(int detaliuComandaId)
        {
            var entity = GetAll().FirstOrDefault(x => x.DetaliuComanda.DetaliuComandaId == detaliuComandaId);
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }
    }
}
