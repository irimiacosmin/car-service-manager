﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class MaterialService : ServiceInterface<Material>
    {
        readonly MaterialRepository _repository;

        public MaterialService(Model1Container dbContext)
        {
            this._repository = new MaterialRepository(dbContext);
        }

        public Material Add(Material entity)
        {
            if (_repository.GetById(entity.MaterialId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Material> GetAll()
        {
            IEnumerable<Material> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public Material GetById(int id)
        {
            Material material = _repository.GetById(id);
            if (material == null)
            {
                throw new EntityGetException();
            }
            return material;
        }

        public void Update(Material entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public Material GetByDenumire(string denumire)
        {
            var entity = GetAll().FirstOrDefault(x => x.Denumire.ToLower().Equals(denumire.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }

        public IEnumerable<Material> GetByDetaliiComandaId(int detaliiComandaId)
        {
            IEnumerable<Material> list = GetAll().Where(x => x.DetaliuComanda.DetaliuComandaId == detaliiComandaId);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public IEnumerable<Material> GetByData(DateTime data)
        {
            IEnumerable<Material> list = GetAll().Where(x => x.DataAprovizionare.Equals(data));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }
    }
}
