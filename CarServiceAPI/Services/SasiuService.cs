﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class SasiuService : ServiceInterface<Sasiu>
    {
        readonly SasiuRepository _repository;

        public SasiuService(Model1Container dbContext)
        {
            this._repository = new SasiuRepository(dbContext);
        }


        public Sasiu Add(Sasiu entity)
        {
            if (_repository.GetById(entity.SasiuId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Sasiu> GetAll()
        {
            IEnumerable<Sasiu> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public Sasiu GetById(int id)
        {
            Sasiu sasiu = _repository.GetById(id);
            if (sasiu == null)
            {
                throw new EntityGetException();
            }
            return sasiu;
        }

        public void Update(Sasiu entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public Sasiu GetByDenumire(string denumire)
        {
            var entity = GetAll().FirstOrDefault(x => x.Denumire.ToLower().Equals(denumire.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }

        public Sasiu GetByCodSasiu(string codSasiu)
        {
            var entity = GetAll().FirstOrDefault(x => x.CodSasiu.ToLower().Equals(codSasiu.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }
    }
}
