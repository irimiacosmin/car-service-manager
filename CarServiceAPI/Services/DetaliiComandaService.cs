﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class DetaliiComandaService : ServiceInterface<DetaliuComanda>
    {
        readonly DetaliiComandaRepository _repository;

        public DetaliiComandaService(Model1Container dbContext)
        {
            this._repository = new DetaliiComandaRepository(dbContext);
        }

        public DetaliuComanda Add(DetaliuComanda entity)
        {
            if (_repository.GetById(entity.DetaliuComandaId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<DetaliuComanda> GetAll()
        {
            IEnumerable<DetaliuComanda> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public DetaliuComanda GetById(int id)
        {
            DetaliuComanda detaliuComanda = _repository.GetById(id);
            if (detaliuComanda == null)
            {
                throw new EntityGetException();
            }
            return detaliuComanda;
        }

        public void Update(DetaliuComanda entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public DetaliuComanda GetByComandaId(int comandaId)
        {
            var entity = GetAll().FirstOrDefault(x => x.Comanda.ComandaId == comandaId);
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }
    }
}
