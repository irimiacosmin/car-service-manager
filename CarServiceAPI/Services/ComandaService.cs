﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class ComandaService : ServiceInterface<Comanda>
    {
        readonly ComandaRepository _repository;

        public ComandaService(Model1Container dbContext)
        {
            this._repository = new ComandaRepository(dbContext);
        }

        public Comanda Add(Comanda entity)
        {
            if (_repository.GetById(entity.ComandaId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Comanda> GetAll()
        {
            IEnumerable<Comanda> list = _repository.GetAll().ToList();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public Comanda GetById(int id)
        {
            Comanda comanda = _repository.GetById(id);
            if (comanda == null)
            {
                throw new EntityGetException();
            }
            return comanda;
        }

        public void Update(Comanda entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public IEnumerable<Comanda> GetByAutoId(int autoId)
        {
            return _repository.GetAll().Where(x => x.AutoAutoId == autoId).ToList();
        }

        public IEnumerable<Comanda> GetByClientId(int clientId)
        {
            IEnumerable<Comanda> list = GetAll().Where(x => x.Client.ClientId == clientId);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Comanda> GetByStareComanda(int stareComanda)
        {
            IEnumerable<Comanda> list = GetAll().Where(x => x.StareComanda.StareComandaId == stareComanda);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Comanda> GetByData(DateTime data)
        {
            IEnumerable<Comanda> list = GetAll().Where(x => x.DataSystem.Equals(data));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Comanda> GetByData(DateTime dataStart, DateTime dataSfarsit)
        {
            IEnumerable<Comanda> list = GetAll().Where(x => x.DataSystem >= dataStart && x.DataProgramare <= dataSfarsit);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Comanda> GetByValoarePiese(decimal min, decimal max)
        {
            IEnumerable<Comanda> list = GetAll().Where(x => x.ValoarePiese >= min && x.ValoarePiese <= max);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public decimal? GetPriceForComanda(int comandaId)
        {
            return _repository.GetPriceForComanda(comandaId);
        }

    }
}
