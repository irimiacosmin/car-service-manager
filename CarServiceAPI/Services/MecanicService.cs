﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class MecanicService : ServiceInterface<Mecanic>
    {
        readonly MecanicRepository _repository;

        public MecanicService(Model1Container dbContext)
        {
            this._repository = new MecanicRepository(dbContext);
        }

        public Mecanic Add(Mecanic entity)
        {
            if (_repository.GetById(entity.MecanicId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Mecanic> GetAll()
        {
            IEnumerable<Mecanic> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;

        }

        public Mecanic GetById(int id)
        {
            Mecanic mecanic = _repository.GetById(id);
            if (mecanic == null)
            {
                throw new EntityGetException();
            }
            return mecanic;
        }

        public void Update(Mecanic entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public IEnumerable<Mecanic> GetByNume(string nume)
        {
            IEnumerable<Mecanic> list = GetAll().Where(x => x.Nume.ToLower().Equals(nume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Mecanic> GetByPrenume(string prenume)
        {
            IEnumerable<Mecanic> list = GetAll().Where(x => x.Prenume.ToLower().Equals(prenume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Mecanic> GetByFullName(string nume, string prenume)
        {
            IEnumerable<Mecanic> list = GetAll().Where(x => x.Nume.ToLower().Equals(nume.ToLower()) && x.Prenume.ToLower().Equals(prenume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Mecanic> GetByDetaliuComandaId(int detaliuComandaId)
        {
            IEnumerable<Mecanic> list = GetAll().Where(x => x.DetaliuComandaId == detaliuComandaId);
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }
    }
}
