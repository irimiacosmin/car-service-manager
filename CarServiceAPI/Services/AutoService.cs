﻿using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using CarServiceAPI.Exceptions;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class AutoService : ServiceInterface<Auto>
    {
        readonly AutoRepository _repository;

        public AutoService(Model1Container dbContext)
        {
            this._repository = new AutoRepository(dbContext);
        }

        public Auto Add(Auto entity) 
        {
            if (_repository.GetById(entity.AutoId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }


        public IEnumerable<Auto> GetAll()
        {
            IEnumerable<Auto> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public Auto GetById(int id)
        {
            Auto auto = _repository.GetById(id);
            if(auto == null)
            {
                throw new EntityGetException();
            }
            return auto;
        }

        public void Update(Auto entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public Auto GetByNumarDeInmatriculare(string numarAuto)
        {
            Auto auto = GetAll().SingleOrDefault(x =>(x.NumarAuto.ToLower().Equals(numarAuto.ToLower()) 
                                                || x.NumarAuto.ToLower().Replace("-", "").Equals(numarAuto.ToLower())));
            if (auto == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return auto;
            }
        }

        public Auto GetBySerieSasiu(string serieSasiu)
        {
            Auto auto = GetAll().SingleOrDefault(x => x.SerieSasiu.ToLower().Equals(serieSasiu.ToLower()));
            if (auto == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return auto;
            }
        }

        public IEnumerable<Auto> GetByClientId(int clientId)
        {
            IEnumerable<Auto> list = GetAll().Where(x => x.ClientId == clientId);
            if (list.Count().Equals(0))
            {
                list = new List<Auto>();
            }
            return list;
        }
    }
}
