﻿using CarServiceAPI.Exceptions;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Repositories;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Services
{
    public class ClientService : ServiceInterface<Client>
    {
        readonly ClientRepository _repository;

        public ClientService(Model1Container dbContext)
        {
            this._repository = new ClientRepository(dbContext);
        }

        public Client Add(Client entity)
        {
            if (_repository.GetById(entity.ClientId) != null)
            {
                throw new EntityAddException("Entity already exist.");
            }
            return _repository.Add(entity);
        }

        public IEnumerable<Client> GetAll()
        {
            IEnumerable<Client> list = _repository.GetAll();
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public Client GetById(int id)
        {
            Client client = _repository.GetById(id);
            if (client == null)
            {
                throw new EntityGetException();
            }
            return client;
        }

        public void Update(Client entity)
        {
            if (!_repository.Update(entity))
                throw new EntityUpdateException();
        }

        public void Delete(int id)
        {
            if (!_repository.Delete(id))
                throw new EntityDeleteException();
        }

        public IEnumerable<Client> GetByNume(string nume)
        {
            IEnumerable<Client> list = GetAll().Where(x => x.Nume.ToLower().Equals(nume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Client> GetByPrenume(string prenume)
        {
            IEnumerable<Client> list = GetAll().Where(x => x.Prenume.ToLower().Equals(prenume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public IEnumerable<Client> GetByFullName(string nume, string prenume)
        {
            IEnumerable<Client> list = GetAll().Where(x => x.Nume.ToLower().Equals(nume.ToLower()) && x.Prenume.ToLower().Equals(prenume.ToLower()));
            if (list.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return list;
        }

        public Client GetByNumarDeTelefon(string numarDeTelefon)
        {
            var entity = GetAll().SingleOrDefault(x => x.Telefon.ToLower().Equals(numarDeTelefon.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }

        public Client GetByEmail(string email)
        {
            var entity = GetAll().SingleOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            if (entity == null)
            {
                throw new EntityGetException();
            }
            else
            {
                return entity;
            }
        }

        public IEnumerable<Client> GetByJudet(string judet)
        {
            Console.WriteLine(GetAll().Count());

            var list = GetAll().Where(x => x.Judet.Equals(judet));
            var byJudet = list.ToList();
            Console.WriteLine(byJudet.Count());
            if (list == null || byJudet.Count().Equals(0))
            {
                throw new EntityGetException();
            }
            return byJudet;
        }
    }
}
