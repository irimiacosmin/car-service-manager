﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Interfaces
{
    interface RepoInterface<T>
    {
        T Add(T entity);
        T GetById(int id);
        IEnumerable<T> GetAll();
        bool Update(T entity);
        bool Delete(int id);
    }
}
