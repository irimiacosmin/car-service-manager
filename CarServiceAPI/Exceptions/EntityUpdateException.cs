﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Exceptions
{
    public class EntityUpdateException : SystemException
    {
        public EntityUpdateException()
        {
        }

        public EntityUpdateException(string message) : base(message)
        {
        }

        public EntityUpdateException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityUpdateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
