﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Exceptions
{
    public class EntityDeleteException : SystemException
    {
        public EntityDeleteException()
        {
        }

        public EntityDeleteException(string message) : base(message)
        {
        }

        public EntityDeleteException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityDeleteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
