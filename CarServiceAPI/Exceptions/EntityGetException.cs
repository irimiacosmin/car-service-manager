﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Exceptions
{
    public class EntityGetException : SystemException
    {
        public EntityGetException()
        {
        }

        public EntityGetException(string message) : base(message)
        {
        }

        public EntityGetException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityGetException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
