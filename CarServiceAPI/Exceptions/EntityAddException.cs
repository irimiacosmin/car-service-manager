﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Exceptions
{
    public class EntityAddException : SystemException
    {
        public EntityAddException()
        {
        }

        public EntityAddException(string message) : base(message)
        {
        }

        public EntityAddException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EntityAddException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
