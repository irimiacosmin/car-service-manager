﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class StareComandaRepository : RepoInterface<StareComanda>
    {
        private readonly Model1Container _dbContext;

        public StareComandaRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public StareComanda Add(StareComanda entity)
        {
            StareComanda thing = _dbContext.StariComanda.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public StareComanda GetById(int id)
        {
            return _dbContext.StariComanda.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.StareComandaId == id);
        }

        public IEnumerable<StareComanda> GetAll()
        {
            return _dbContext.StariComanda.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(StareComanda entity)
        {
            try
            {
                StareComanda stareComanda = GetById(entity.StareComandaId);
                stareComanda = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                StareComanda stareComanda = GetById(id);
                stareComanda.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
