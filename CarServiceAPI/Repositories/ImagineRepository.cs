﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class ImagineRepository : RepoInterface<Imagine>
    {
        private readonly Model1Container _dbContext;

        public ImagineRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Imagine Add(Imagine entity)
        {
            Imagine thing = _dbContext.Images.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Imagine GetById(int id)
        {
            return _dbContext.Images.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.ImagineId == id);
        }

        public IEnumerable<Imagine> GetAll()
        {
            return _dbContext.Images.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Imagine entity)
        {
            try
            {
                Imagine imagine = GetById(entity.ImagineId);
                imagine = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Imagine imagine = GetById(id);
                imagine.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
