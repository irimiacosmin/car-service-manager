﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class OperatieRepository : RepoInterface<Operatie>
    {
        private readonly Model1Container _dbContext;

        public OperatieRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Operatie Add(Operatie entity)
        {
            Operatie thing = _dbContext.Operations.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Operatie GetById(int id)
        {
            return _dbContext.Operations.AsNoTracking().FirstOrDefault(x => x.isDeleted == false && x.OperatieId == id);
        }

        public IEnumerable<Operatie> GetAll()
        {
            return _dbContext.Operations.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Operatie entity)
        {
            try
            {
                Operatie operatie = GetById(entity.OperatieId);
                operatie = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Operatie operatie = GetById(id);
                operatie.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
