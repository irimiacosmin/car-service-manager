﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class MaterialRepository : RepoInterface<Material>
    {
    private readonly Model1Container _dbContext;

    public MaterialRepository(Model1Container dbContext)
    {
        this._dbContext = dbContext;
    }

        public Material Add(Material entity)
    {
        Material thing = _dbContext.Materials.Add(entity);
        _dbContext.SaveChanges();
        return thing;
    }

    public Material GetById(int id)
    {
        return _dbContext.Materials.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.MaterialId == id);
    }

    public IEnumerable<Material> GetAll()
    {
        return _dbContext.Materials.Where(x => x.isDeleted == false).AsNoTracking();
    }

    public bool Update(Material entity)
    {
        try
        {
            Material material = GetById(entity.MaterialId);
            material = entity;
            _dbContext.SaveChanges();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public bool Delete(int id)
    {
        try
        {
            Material material = GetById(id);
            material.isDeleted = true;
            _dbContext.SaveChanges();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
    }
}
