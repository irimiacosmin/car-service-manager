﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class ClientRepository : RepoInterface<Client>
    {
        private readonly Model1Container _dbContext;

        public ClientRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Client Add(Client entity)
        {
            Client thing = _dbContext.Clients.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Client GetById(int id)
        {
            return _dbContext.Clients.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.ClientId == id);
        }

        public IEnumerable<Client> GetAll()
        {
            return _dbContext.Clients.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Client entity)
        {
            try
            {
                Client client = GetById(entity.ClientId);
                client = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Client client = GetById(id);
                client.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
