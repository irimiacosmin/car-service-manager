﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class ComandaRepository : RepoInterface<Comanda>
    {
        private readonly Model1Container _dbContext;

        public ComandaRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Comanda Add(Comanda entity)
        {
            Comanda thing = _dbContext.Comands.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Comanda GetById(int id)
        {
            return _dbContext.Comands.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.ComandaId == id);
        }

        public IEnumerable<Comanda> GetAll()
        {
            return _dbContext.Comands.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Comanda entity)
        {
            try
            {
                Comanda comanda = GetById(entity.ComandaId);
                comanda = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Comanda comanda = GetById(id);
                comanda.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public decimal? GetPriceForComanda(int comandaId)
        {
            decimal? price = 0;

            var detaliiComanda = _dbContext.DetaliiComenzi.Where(x => x.Comanda.ComandaId == comandaId);

            foreach (var detaliu in detaliiComanda)
            {
                var listMateriale =
                    _dbContext.Materials.Where(x => x.DetaliuComandaId == detaliu.DetaliuComandaId);
                foreach (var material in listMateriale)
                {
                    price += material.Cantitate * material.Pret;
                }
            }

            return price;
        }


    }
}
