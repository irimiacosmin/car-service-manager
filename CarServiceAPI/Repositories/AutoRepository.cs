﻿using ModelDesignFirst_Laborator1;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using CarServiceAPI.Interfaces;
using CarServiceAPI.Exceptions;
using System;

namespace CarServiceAPI.Repositories
{
    public class AutoRepository : RepoInterface<Auto>
    {
        private readonly Model1Container _dbContext;

        public AutoRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Auto Add(Auto entity)
        {
            Auto thing = _dbContext.Autos.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Auto GetById(int id)
        {
            return _dbContext.Autos.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.AutoId == id);
        }

        public IEnumerable<Auto> GetAll()
        {
            return _dbContext.Autos.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Auto entity)
        {
            try
            {
                Auto auto = GetById(entity.AutoId);
                auto = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Auto auto = GetById(id);
                auto.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
