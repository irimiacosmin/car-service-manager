﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class DetaliiComandaRepository : RepoInterface<DetaliuComanda>
    {
        private readonly Model1Container _dbContext;


        public DetaliiComandaRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public DetaliuComanda Add(DetaliuComanda entity)
        {
            DetaliuComanda thing = _dbContext.DetaliiComenzi.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public DetaliuComanda GetById(int id)
        {
            return _dbContext.DetaliiComenzi.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.DetaliuComandaId == id);
        }

        public IEnumerable<DetaliuComanda> GetAll()
        {
            return _dbContext.DetaliiComenzi.Where(x => x.isDeleted == false);
        }

        public bool Update(DetaliuComanda entity)
        {
            try
            {
                DetaliuComanda detaliuComanda = GetById(entity.DetaliuComandaId);
                detaliuComanda = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                DetaliuComanda detaliuComanda = GetById(id);
                detaliuComanda.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
