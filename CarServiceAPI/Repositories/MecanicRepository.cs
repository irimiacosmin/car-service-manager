﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class MecanicRepository : RepoInterface<Mecanic>
    {
        private readonly Model1Container _dbContext;

        public MecanicRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Mecanic Add(Mecanic entity)
        {
            Mecanic thing = _dbContext.Mecanics.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Mecanic GetById(int id)
        {
            return _dbContext.Mecanics.AsNoTracking().SingleOrDefault(x => x.isDeleted == false && x.MecanicId == id);
        }

        public IEnumerable<Mecanic> GetAll()
        {
            return _dbContext.Mecanics.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Mecanic entity)
        {
            try
            {
                Mecanic mecanic = GetById(entity.MecanicId);
                mecanic = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Mecanic mecanic = GetById(id);
                mecanic.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
