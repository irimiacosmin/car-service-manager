﻿using CarServiceAPI.Interfaces;
using ModelDesignFirst_Laborator1;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarServiceAPI.Repositories
{
    class SasiuRepository : RepoInterface<Sasiu>
    {
        private readonly Model1Container _dbContext;

        public SasiuRepository(Model1Container dbContext)
        {
            this._dbContext = dbContext;
        }

        public Sasiu Add(Sasiu entity)
        {
            Sasiu thing = _dbContext.Sasiuri.Add(entity);
            _dbContext.SaveChanges();
            return thing;
        }

        public Sasiu GetById(int id)
        {
            return _dbContext.Sasiuri.AsNoTracking().FirstOrDefault(x => x.isDeleted == false && x.SasiuId == id);
        }

        public IEnumerable<Sasiu> GetAll()
        {
            return _dbContext.Sasiuri.Where(x => x.isDeleted == false).AsNoTracking();
        }

        public bool Update(Sasiu entity)
        {
            try
            {
                Sasiu sasiu = GetById(entity.SasiuId);
                sasiu = entity;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Sasiu sasiu = GetById(id);
                sasiu.isDeleted = true;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
