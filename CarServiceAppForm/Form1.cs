﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CarServiceAPI.Exceptions;
using CarServiceAPI.Services;
using ModelDesignFirst_Laborator1;


namespace CarServiceAppForm
{

    public partial class form : Form
    {
        private static Model1Container _dbContext = new Model1Container();
        private  AutoService _autoService = new AutoService(_dbContext);
        private  ClientService _clientService = new ClientService(_dbContext);
        private  ComandaService _comandaService = new ComandaService(_dbContext);
        private  DetaliiComandaService _detaliiComandaService = new DetaliiComandaService(_dbContext);
        private  ImagineService _imagineService = new ImagineService(_dbContext);
        private  MaterialService _materialService = new MaterialService(_dbContext);
        private  MecanicService _mecanicService = new MecanicService(_dbContext);
        private  OperatieService _operatieService = new OperatieService(_dbContext);
        private  SasiuService _sasiuService = new SasiuService(_dbContext);
        private  StareComandaService _stareComandaService = new StareComandaService(_dbContext);

        private Client currentClient = null;
        private Auto currentAuto = null;
        private Comanda currentComanda = null;
        
        private bool _focusOnClient = true;
        private bool _focusOnAuto = false;
        private bool _focusOnComanda = false;


        public form()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage1);
            tabControl1.TabPages.Remove(tabPage2);
            tabControl1.TabPages.Remove(tabPage3);
            tabControl1.TabPages.Remove(tabPage4);
            tabControl1.TabPages.Remove(tabPage5);
            tabControl1.TabPages.Remove(tabPage6);
            tabControl1.TabPages.Remove(tabPage7);
            tabControl1.TabPages.Remove(tabPage8);
            RePopulateClientList(null);
            DataGridViewComboBoxColumn comboMecanici = new DataGridViewComboBoxColumn();
            {
                comboMecanici.DataSource = _mecanicService.GetAll().ToList();
                comboMecanici.Name = "DetaliiMecanici";
                comboMecanici.HeaderText = "Mecanici";
                comboMecanici.DisplayMember = "Nume";
                comboMecanici.ValueMember = "Prenume";
                this.dataGridView4.Columns.Add(comboMecanici);
            }

            DataGridViewButtonColumn buttonImagini = new DataGridViewButtonColumn();
            {
                buttonImagini.Name = "DetaliiImagini";
                buttonImagini.HeaderText = "Imagini";
                buttonImagini.Text = "Detalii Imagini";
                buttonImagini.UseColumnTextForButtonValue = true; //dont forget this line
                this.dataGridView4.Columns.Add(buttonImagini);
            }

            DataGridViewButtonColumn buttonMateriale = new DataGridViewButtonColumn();
            {
                buttonMateriale.Name = "DetaliiMateriale";
                buttonMateriale.HeaderText = "Materiale";
                buttonMateriale.Text = "Detalii Materiale";
                buttonMateriale.UseColumnTextForButtonValue = true; //dont forget this line
                this.dataGridView4.Columns.Add(buttonMateriale);
            }
            DataGridViewButtonColumn adaugaMateriale = new DataGridViewButtonColumn();
            {
                adaugaMateriale.Name = "AdaugaMaterial";
                adaugaMateriale.HeaderText = "";
                adaugaMateriale.Text = "+";
                adaugaMateriale.Width = 30;
                adaugaMateriale.UseColumnTextForButtonValue = true;
                this.dataGridView4.Columns.Add(adaugaMateriale);
            }
        }

        private void RePopulateClientList(String startsWith)
        {
            dataGridView1.Rows.Clear();
            List<Client> listForPopulate;

            if (startsWith == null)
            {
                listForPopulate = _clientService.GetAll().ToList();
            }
            else
            {
                listForPopulate = _clientService.GetAll().Where(x =>
                    x.Nume.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.Prenume.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.ClientId.ToString().ToLower().StartsWith(startsWith.ToLower())).ToList();
            }

            if (listForPopulate.Count == 0)
            {
                button10.Enabled = false;
                button11.Enabled = false;
                button12.Enabled = false;
                button5.Enabled = false;
                button7.Enabled = false;
            }
            else
            {
                ReFillAutoList(listForPopulate.ElementAt(0).ClientId);
                foreach (var client in listForPopulate)
                {
                    dataGridView1.Rows.Add(client.ClientId.ToString(), client.Nume, client.Prenume);
                }
                button10.Enabled = true;
            }
            

        }

        private void ReFillClientList(int autoId)
        {
            dataGridView1.Rows.Clear();

            try
            {
                var client = _clientService.GetById(_autoService.GetById(autoId).ClientId);
                dataGridView1.Rows.Add(client.ClientId, client.Nume, client.Prenume);
                button10.Enabled = true;
            }
            catch (EntityGetException)
            {
                button10.Enabled = false;
            
            }

        }

        private void ReFillAutoList(int clientId)
        {
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            try
            {
                var listAuto = _autoService.GetByClientId(clientId).ToList();
                if(_focusOnClient)
                    ReFillComenziList(listAuto.ElementAt(0).AutoId);
              
                foreach (var auto in listAuto)
                {
                    dataGridView2.Rows.Add(auto.AutoId, auto.NumarAuto, auto.SerieSasiu);
                }
                button11.Enabled = true;
                button5.Enabled = true;
            }
            catch (EntityGetException)
            {
                button11.Enabled = false;
                button12.Enabled = false;
                button7.Enabled = false;

            }

        }
        private void ReFillComenziList(int autoId)
        {
            dataGridView3.Rows.Clear();
            try
            {
                var listComanda = _comandaService.GetByAutoId(autoId).ToList();
                foreach (var comanda in listComanda)
                {
                    dataGridView3.Rows.Add(comanda.ComandaId, comanda.Descriere, comanda.ValoarePiese, comanda.KmBord);
                }
                button12.Enabled = true;
                button7.Enabled = true;
            }
            catch (EntityGetException)
            {
                button12.Enabled = false;

            }

        }

        private void RePopulateAutoList(String startsWith)
        {
            dataGridView2.Rows.Clear();
            List<Auto> listForPopulate;

            if (startsWith == null)
            {
                listForPopulate = _autoService.GetAll().ToList();
            }
            else
            {
                listForPopulate = _autoService.GetAll().Where(x =>
                    x.NumarAuto.ToLower().StartsWith(startsWith.ToLower()) ||
                    x.SerieSasiu.ToLower().StartsWith(startsWith.ToLower())||
                    x.AutoId.ToString().ToLower().StartsWith(startsWith.ToLower())).ToList();
            }

            if (listForPopulate.Count == 0)
            {
                button10.Enabled = false;
                button11.Enabled = false;
                button12.Enabled = false;
            }
            else
            {
                button11.Enabled = true;
                ReFillClientList(listForPopulate.ElementAt(0).AutoId);
                foreach (var auto in listForPopulate)
                {
                    dataGridView2.Rows.Add(auto.AutoId, auto.NumarAuto, auto.SerieSasiu);
                }
            }
            

        }


        private int GetFocus()
        {
            if (_focusOnClient)
            {
                return 1;
            }else if (_focusOnAuto)
            {
                return 2;
            }else if(_focusOnComanda)
            {
                return 3;
            }

            return 0;
        }

        private void SetFocus(bool focusOnClient, bool focusOnAuto, bool focusOnComanda)
        {
            this._focusOnClient = focusOnClient;
            this._focusOnAuto = focusOnAuto;
            this._focusOnComanda = focusOnComanda;
        }

        private void ClearAllLists()
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ClearAllLists();
            textBox2.Text = "";
            SetFocus(true, false, false);
            RePopulateClientList(textBox1.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            ClearAllLists();
            textBox1.Text = "";
            SetFocus(false, true, false);
            RePopulateAutoList(textBox2.Text);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var clientId = Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString());

            switch (GetFocus())
            {
                case 1:
                {
                    ReFillAutoList(clientId);
                    break;
                }
                case 2:
                {
                    break;
                }
                case 3:
                {
                    break;
                }
                default: break;
            }


            
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var autoId = Int32.Parse(dataGridView2.SelectedCells[0].Value.ToString());
            switch (GetFocus())
            {
                case 1:
                {
                    ReFillComenziList(autoId);
                        break;
                }
                case 2:
                {
                    ReFillClientList(autoId);
                    ReFillComenziList(autoId);
                        break;
                }
                case 3:
                {
                    break;
                }
                default: break;
            }
            
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var comandaId = Int32.Parse(dataGridView3.SelectedCells[0].Value.ToString());
            switch (GetFocus())
            {
                case 1:
                {
                    break;
                }
                case 2:
                {
                    break;
                }
                case 3:
                {
                    ReFillClientList(e.RowIndex);
                    ReFillAutoList(e.RowIndex);
                        break;
                }
                default: break;
            }

        }


        private void autoList_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }


        private void listaClienti_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Clients_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            
            if (!tabControl1.TabPages.Contains(tabPage4))
                tabControl1.TabPages.Add(tabPage4);
            tabControl1.SelectTab(tabPage4);
            currentClient = _clientService.GetById(Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString()));

            textBox22.Text = currentClient.Nume;
            textBox21.Text = currentClient.Prenume;
            textBox20.Text = currentClient.Adresa;
            textBox19.Text = currentClient.Localitate;
            textBox18.Text = currentClient.Judet;
            textBox11.Text = currentClient.Telefon;
            textBox10.Text = currentClient.Email;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!tabControl1.TabPages.Contains(tabPage1))
                tabControl1.TabPages.Add(tabPage1);
            tabControl1.SelectTab(tabPage1);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        void numeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            string[] permisiveNumbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            if (e.KeyChar == 8 || permisiveNumbers.Any(e.KeyChar.ToString().Contains))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
            return;
        }

        void characters_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8 || (e.KeyChar >= 65 && e.KeyChar <= 90) || (e.KeyChar >= 97 && e.KeyChar <= 122))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
            return;
        }

        void characters_and_numerics_KeyPress(object sender, KeyPressEventArgs e)
        {
            string[] permisiveNumbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

            if (e.KeyChar == 8 || (e.KeyChar >= 65 && e.KeyChar <= 90) || (e.KeyChar >= 97 && e.KeyChar <= 122) || permisiveNumbers.Any(e.KeyChar.ToString().Contains))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
            return;
        }

        void characters_Space_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8 || e.KeyChar == 32 || e.KeyChar == 46 || e.KeyChar == 45 || (e.KeyChar >= 65 && e.KeyChar <= 90) || (e.KeyChar >= 97 && e.KeyChar <= 122))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
            return;
        }

        void characters_Email_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 8 || e.KeyChar == 46 || e.KeyChar == 64 || (e.KeyChar >= 65 && e.KeyChar <= 90) || (e.KeyChar >= 97 && e.KeyChar <= 122))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
            return;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (!tabControl1.TabPages.Contains(tabPage5))
                tabControl1.TabPages.Add(tabPage5);
            tabControl1.SelectTab(tabPage5);

            currentClient = _clientService.GetById(Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString()));
            currentAuto = _autoService.GetById(Int32.Parse(dataGridView2.SelectedCells[0].Value.ToString()));
            //currentComanda = _comandaService.GetById(Int32.Parse(dataGridView3.SelectedCells[0].Value.ToString()));
            label50.Text = currentClient.ClientId.ToString();
            label48.Text = currentClient.Nume;
            label47.Text = currentClient.Prenume;
            textBox26.Text = currentAuto.NumarAuto;
            textBox25.Text = currentAuto.SerieSasiu;

        
            try
            {
                var currentSasiu = currentAuto.Sasiu;
                textBox24.Text = currentAuto.Sasiu.CodSasiu;
                textBox23.Text = currentAuto.Sasiu.Denumire;
                textBox24.Visible = true;
                textBox23.Visible = true;
                label49.Visible = true;
                label46.Visible = true;
            }
            catch (InvalidOperationException)
            {
                textBox24.Visible = false;
                textBox23.Visible = false;
                label49.Visible = false;
                label46.Visible = false;
            }
            

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (!tabControl1.TabPages.Contains(tabPage6))
                tabControl1.TabPages.Add(tabPage6);
            tabControl1.SelectTab(tabPage6);

            currentClient = _clientService.GetById(Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString()));
            currentAuto = _autoService.GetById(Int32.Parse(dataGridView2.SelectedCells[0].Value.ToString()));
            currentComanda = _comandaService.GetById(Int32.Parse(dataGridView3.SelectedCells[0].Value.ToString()));
            dataGridView4.Rows.Clear();
            var detaliuComanda = _detaliiComandaService.GetByComandaId(currentComanda.ComandaId);

            label68.Text = currentClient.ClientId.ToString();
            label67.Text = currentClient.Nume;
            label67.Text = currentClient.Prenume;
            label62.Text = currentAuto.AutoId.ToString();
            label61.Text = currentAuto.NumarAuto;
            label60.Text = currentAuto.SerieSasiu;

            textBox28.Text = currentComanda.KmBord.ToString();
            textBox27.Text = currentComanda.Descriere;
            dateTimePicker2.Value = currentComanda.DataProgramare;
            try
            {
                var mecanici = _mecanicService.GetByDetaliuComandaId(detaliuComanda.DetaliuComandaId).ToList();
                var lenOperatii = detaliuComanda.Operaties.Count();
                var operatii = detaliuComanda.Operaties.ToList();
                for(int i=0; i< lenOperatii; i++)
                {
                    dataGridView4.Rows.Add(operatii.ElementAt(i).Denumire, operatii.ElementAt(i).TimpExecutie);
                    dataGridView4.Rows[i].Cells["DetaliiMecanici"].Value = mecanici.ElementAt(i).Prenume;
                    dataGridView4.Rows[i].Cells["DetaliiMecanici"].ReadOnly = true;
                    var hiddenStyle = new DataGridViewCellStyle();
                    hiddenStyle.Padding = new Padding(100, 0, 0, 0);
                    dataGridView4.Rows[i].Cells["AdaugaMaterial"].Style = hiddenStyle;

                }
            }
            catch (EntityGetException) {}
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!tabControl1.TabPages.Contains(tabPage2))
                tabControl1.TabPages.Add(tabPage2);
            tabControl1.SelectTab(tabPage2);
          

            currentClient = _clientService.GetById(Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString()));
            label15.Text = currentClient.ClientId.ToString();
            label19.Text = currentClient.Nume;
            label20.Text = currentClient.Prenume;


        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!tabControl1.TabPages.Contains(tabPage3))
                tabControl1.TabPages.Add(tabPage3);
            tabControl1.SelectTab(tabPage3);

            currentClient = _clientService.GetById(Int32.Parse(dataGridView1.SelectedCells[0].Value.ToString()));
            currentAuto = _autoService.GetById(Int32.Parse(dataGridView2.SelectedCells[0].Value.ToString()));
            label23.Text = currentClient.ClientId.ToString();
            label22.Text = currentClient.Nume;
            label21.Text = currentClient.Prenume;
            label33.Text = currentAuto.AutoId.ToString();
            label32.Text = currentAuto.NumarAuto;
            label31.Text = currentAuto.SerieSasiu;
            
        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(Clients);
            tabControl1.TabPages.Remove(tabPage1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(Clients);
            tabControl1.TabPages.Remove(tabPage2);
            
        }

        private void button14_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(Clients);
            tabControl1.TabPages.Remove(tabPage3);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox13.Text = "";
            textBox12.Text = "";
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var newClient = new Client();
            newClient.Nume = textBox3.Text;
            newClient.Prenume = textBox4.Text;
            newClient.Adresa = textBox5.Text;
            newClient.Localitate = textBox8.Text;
            newClient.Judet = textBox7.Text;
            newClient.Telefon = textBox6.Text;
            newClient.Email = textBox9.Text;
            newClient.isDeleted = false;
            if (Utils.isValidClient(newClient))
            {
                try
                {
                    _clientService.Add(newClient);
                    MessageBox.Show("Clientul a fost adaugat cu succes!");

                }
                catch (EntityAddException)
                {
                    MessageBox.Show("Clientul nu a putut fi adaugat!");
                }
            }

        }

        private void button15_Click(object sender, EventArgs e)
        {
            var newSasiu = new Sasiu();
            newSasiu.CodSasiu = textBox13.Text;
            newSasiu.Denumire = textBox12.Text;
            newSasiu.isDeleted = false;
            try
            {
                _sasiuService.Add(newSasiu);
                MessageBox.Show("Sasiul a fost adaugat cu succes!");
            }
            catch (EntityAddException)
            {
                MessageBox.Show("Sasiul nu a putut fi adaugat!");

            }

        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var newAuto = new Auto();
            newAuto.NumarAuto = textBox16.Text;
            newAuto.SerieSasiu = textBox15.Text;

            var newSasiu = new Sasiu();
            newSasiu.CodSasiu = textBox13.Text;
            newSasiu.Denumire = textBox12.Text;
            newSasiu.isDeleted = false;

            newAuto.Sasiu = newSasiu;
            newAuto.ClientId = currentClient.ClientId;
            newAuto.isDeleted = false;
            if (Utils.isValidAuto(newAuto))
            {
                try
                {
                    _autoService.Add(newAuto);
                    MessageBox.Show("Autoturismul a fost adaugat cu succes!");
                }
                catch (EntityAddException)
                {
                    MessageBox.Show("Autoturismul nu a putut fi adaugat!");

                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var newComanda = new Comanda();
            var kmVal = -1;
            try
            {
                kmVal = Int32.Parse(textBox14.Text);
            }
            catch (Exception){}
            DateTime myDateTime = DateTime.Now;
            //string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            newComanda.KmBord = kmVal;
            newComanda.DataSystem = myDateTime;
            newComanda.DataProgramare = dateTimePicker1.Value.Date;
            newComanda.DataFinalizare = new DateTime(2050,01,01);
            newComanda.Descriere = textBox17.Text;
            newComanda.ValoarePiese = new decimal(0);
            newComanda.StareComanda = _stareComandaService.GetById(1);
            newComanda.Auto = _autoService.GetById(currentAuto.AutoId);
            newComanda.Client = _clientService.GetById(currentClient.ClientId);
            newComanda.isDeleted = false;
           

            try
            {
                _comandaService.Add(newComanda);
                MessageBox.Show("Comanda a fost adaugat cu succes!");
            }
            catch (EntityAddException)
            {
                MessageBox.Show("Comanda nu a putut fi adaugat!");

            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        private void FillImagesForCurrentComanda(int imagineId)
        {
            if (!tabControl1.TabPages.Contains(tabPage7))
                tabControl1.TabPages.Add(tabPage7);
            tabControl1.SelectTab(tabPage7);
            var images = _imagineService.GetByDetaliiComandaId(_detaliiComandaService
                .GetByComandaId(currentComanda.ComandaId).DetaliuComandaId);
            foreach (var img in images)
            {
                dataGridView5.Rows.Add(img.Titlu, img.Descriere, ByteToImage(img.Foto));
            }
        }

        private void FillMaterialsForCurrentComanda(int operatieId)
        {
            if (!tabControl1.TabPages.Contains(tabPage8))
                tabControl1.TabPages.Add(tabPage8);
            tabControl1.SelectTab(tabPage8);
            var materials = _materialService.GetByDetaliiComandaId(_detaliiComandaService
                .GetByComandaId(currentComanda.ComandaId).DetaliuComandaId);

            foreach (var material in materials)
            {
                dataGridView6.Rows.Add(material.MaterialId, material.Denumire, material.Cantitate, material.Pret, material.DataAprovizionare.ToString());
            }

        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)
                {
                    dataGridView5.Rows.Clear();
                    FillImagesForCurrentComanda(e.RowIndex);
                }
                else if(e.ColumnIndex == 4)
                {
                    dataGridView6.Rows.Clear();
                    FillMaterialsForCurrentComanda(e.RowIndex);
                }
                else if(e.ColumnIndex == 5)
                {
                    try
                    {
                        var newOperatie = new Operatie();
                        DataGridViewTextBoxCell ceva = (DataGridViewTextBoxCell)dataGridView4.Rows[e.RowIndex].Cells[0];
                        DataGridViewTextBoxCell ceva1 = (DataGridViewTextBoxCell)dataGridView4.Rows[e.RowIndex].Cells[1];
                        DataGridViewComboBoxCell ceva2 = (DataGridViewComboBoxCell)dataGridView4.Rows[e.RowIndex].Cells[1];
                        newOperatie.Denumire = ceva.Value.ToString();
                        newOperatie.TimpExecutie = decimal.Parse(ceva1.Value.ToString());
                        
                        //MessageBox.Show(ceva.Value.ToString());

                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage4);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage5);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage6);
        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {

        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Esti sigur ca vrei sa stergi clientul?", "ATENTIE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    _clientService.Delete(currentClient.ClientId);
                    MessageBox.Show("Clientul a fost sters cu succes!");
                    tabControl1.TabPages.Remove(tabPage4);
                    RePopulateClientList(null);
                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Clientul nu a putut fi sters!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            var newClient = currentClient;
            newClient.Nume = textBox22.Text;
            newClient.Prenume = textBox21.Text;
            newClient.Adresa = textBox20.Text;
            newClient.Localitate = textBox19.Text;
            newClient.Judet = textBox18.Text;
            newClient.Telefon = textBox11.Text;
            newClient.Email = textBox10.Text;
            newClient.isDeleted = false;

            if (Utils.isValidClient(newClient))
            {
                try
                {
                    _clientService.Update(newClient);
                    MessageBox.Show("Clientul a fost editat cu succes!");
                    tabControl1.TabPages.Remove(tabPage4);
                    RePopulateClientList(null);

                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Clientul nu a putut fi editat!");
                }
            }
           
        }

        private void button16_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Esti sigur ca vrei sa stergi autoturismul?", "ATENTIE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    _autoService.Delete(currentAuto.AutoId);
                    MessageBox.Show("Autoturismul a fost sters cu succes!");
                    tabControl1.TabPages.Remove(tabPage5);
                    RePopulateAutoList(null);
                    RePopulateClientList(null);

                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Autoturismul nu a putut fi sters!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            var newAuto = currentAuto;
            newAuto.NumarAuto = textBox26.Text;
            newAuto.SerieSasiu = textBox25.Text;
            try
            {
                newAuto.Sasiu.CodSasiu = textBox24.Text;
                newAuto.Sasiu.Denumire = textBox23.Text;
            }
            catch (Exception){}
         

            if (Utils.isValidAuto(newAuto))
            {
                try
                {
                    _autoService.Update(newAuto);
                    MessageBox.Show("Autoturismul a fost editat cu succes!");
                    tabControl1.TabPages.Remove(tabPage5);
                    RePopulateAutoList(null);
                    RePopulateClientList(null);

                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Autoturismul nu a putut fi editat!");
                }
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Esti sigur ca vrei sa finalizezi comanda?", "ATENTIE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var newComanda = currentComanda;
                    newComanda.StareComanda = _stareComandaService.GetById(2);
                    newComanda.DataFinalizare = DateTime.Now;
                    _comandaService.Update(newComanda);
                    MessageBox.Show("Comanda a fost finalizata cu succes!");
                    tabControl1.TabPages.Remove(tabPage6);
                    RePopulateAutoList(null);
                    RePopulateClientList(null);
                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Comanda nu a putut fi finalizata!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Esti sigur ca vrei sa stergi comanda?", "ATENTIE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    _comandaService.Delete(currentComanda.ComandaId);
                    MessageBox.Show("Comanda a fost stersa cu succes!");
                    tabControl1.TabPages.Remove(tabPage6);
                    RePopulateAutoList(null);
                    RePopulateClientList(null);    
                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Comanda nu a putut fi stearsa!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Esti sigur ca vrei sa refuzi comanda?", "ATENTIE", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var newComanda = currentComanda;
                    newComanda.StareComanda = _stareComandaService.GetById(3);
                    newComanda.DataFinalizare = DateTime.Now;
                    _comandaService.Update(newComanda);
                    MessageBox.Show("Comanda a fost refuzata cu succes!");
                    tabControl1.TabPages.Remove(tabPage6);
                    RePopulateAutoList(null);
                    RePopulateClientList(null);
                }
                catch (EntityUpdateException)
                {
                    MessageBox.Show("Comanda nu a putut fi refuzata!");
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            textBox29.Text = "";
            textBox30.Text = "";
            pictureBox1.Image = null;
            button24.Visible = false;
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            button24.Visible = true;
            var newImage = new Imagine();
            newImage.Titlu = textBox29.Text;
            newImage.Descriere = textBox30.Text;
            newImage.Data = dateTimePicker3.Value;
            newImage.DetaliuComandaId = _detaliiComandaService
                .GetByComandaId(currentComanda.ComandaId).DetaliuComandaId;
            if (PhotoAsBytes != null)
            {
                newImage.Foto = PhotoAsBytes;

            }
            newImage.isDeleted = false;
            try
            {

                _imagineService.Add(newImage);
                MessageBox.Show("Imaginea a fost adaugata cu succes");
                button24.Visible = true;
                groupBox1.Visible = false;
                dataGridView5.Rows.Add(newImage.Titlu, newImage.Descriere, ByteToImage(newImage.Foto));
            }
            catch (EntityAddException)
            {
                MessageBox.Show("Imaginea nu a putut fi adaugata!");
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage7);
        }

        private byte[] PhotoAsBytes = null;

        private void button26_Click(object sender, EventArgs e)
        {
            try
            {
                string imgLocation = "";
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Filter = "jpg files(*.jpg)|*.jpg| PNG files)(*.png)|*.png"
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    //imgLocation = dialog.FileName;
                    var filename = dialog.FileName;
                    PhotoAsBytes = File.ReadAllBytes(filename);
                    //PictureBox.ImageLocation = imgLocation;
                    pictureBox1.Image = ByteToImage(PhotoAsBytes);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage8);
        }

        private void button31_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
            textBox32.Text = "";
            textBox31.Text = "";
            textBox33.Text = "";
            button31.Visible = false;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            var newMaterial = new Material();
            try
            {
                newMaterial.Denumire = textBox32.Text;
                newMaterial.Cantitate = decimal.Parse(textBox31.Text);
                newMaterial.Pret = decimal.Parse(textBox33.Text);
                newMaterial.DataAprovizionare = dateTimePicker4.Value;
                newMaterial.DetaliuComandaId = _detaliiComandaService
                    .GetByComandaId(currentComanda.ComandaId).DetaliuComandaId;
                newMaterial.isDeleted = false;
                _materialService.Add(newMaterial);
                groupBox2.Visible = false;
                button31.Visible = true;
                dataGridView6.Rows.Add(newMaterial.MaterialId, newMaterial.Denumire, newMaterial.Cantitate,
                    newMaterial.Pret, newMaterial.DataAprovizionare.ToString());
                MessageBox.Show("Materialul a fost adaugat cu succes.");

            }
            catch (EntityAddException)
            {
                MessageBox.Show("Materialul nu a putut fi adaugat.");

            }
            catch (Exception)
            {

                MessageBox.Show("Date invalide au fost adaugate in campuri. Verifica inca o data!");
            }
            
        }

        private void button17_Click(object sender, EventArgs e)
        {
            var newComanda = currentComanda;
            try
            {
                newComanda.KmBord = int.Parse(textBox28.Text);
                newComanda.Descriere = textBox27.Text;
                newComanda.DataProgramare = dateTimePicker2.Value;
                _comandaService.Update(newComanda);
            }
            catch (EntityUpdateException)
            {
                MessageBox.Show("Comanda editata cu succes.");
            }
            catch (Exception)
            {
                MessageBox.Show("Eroare la editae comanda.");
            }

            try
            {
                newComanda.KmBord = int.Parse(textBox28.Text);
                newComanda.Descriere = textBox27.Text;
                newComanda.DataProgramare = dateTimePicker2.Value;
                _comandaService.Update(newComanda);
            }
            catch (EntityUpdateException)
            {
                MessageBox.Show("Comanda editata cu succes.");
            }
            catch (Exception)
            {
                MessageBox.Show("Eroare la editae comanda.");
            }
        }
    }
}
